# EPUB-Adapter Institut Monteclair


## Présentation
Le projet découle d’une demande, de la part l’Institut Montéclair, de réalisation d’une application d’édition de livres virtuels. Cet institut est un établissement médico-social du Groupe VYV géré par le Pôle Accompagnement & Soins de VYV3 Pays de la Loire. Il accueille des enfants souffrant de déficiences visuelles de la naissance jusqu'au terme d'un parcours de formation.
L'objectif du projet était de développer un outil graphique permettant de modifier/remplacer la feuille de style d'un EPUB par une autre. L'éditeur est intuitif et ergonomique tout en permettant de répliquer les mêmes modifications sur un ensemble de livres EPUB de la bibliothèque en fonction des handicaps individuels de chacun des résidents. Ce projet s’inscrit donc dans un processus de transcription déjà initié.
Cette solution : 
- Ne devait pas nécessiter une connaissance de programmation 
- Devait répondre à un besoin d’édition dans le but de l’adaptation de livres 
- Devait avoir une ergonomie et un temps de prise en main rapide et intuitif

La documentation est disponible [ici](https://gitlab.com/api/v4/projects/29846416/repository/files/EPUBAdapter.pdf/raw?ref=main), à la fin du fichier

## Lien de téléchargement

- [Epub-Adapter_2.0.exe](https://gitlab.com/api/v4/projects/29846416/repository/files/target%2FEpub-Adapter_2.0.exe/raw?ref=main)

- [Epub-Adapter_2.0.deb](https://gitlab.com/api/v4/projects/29846416/repository/files/target%2FEpub-Adapter_2.0.deb/raw?ref=main)


## Compilation

Afin de compiler un exécutable d'Epub Adapter, il est nécessaire de créer un artefact au format jar de la version à compiler. La classe principal à exécuter est : **App**

Une fois l'artefact créé, il est nécessaire de spécifier le chemin d'accès au fichier .jar générer dans la configuration maven (pom.xml) :
![Exemple pom.xml](./readme_resources/jar_path_maven.png)


### Prérequis pour générer un .exe (Windows)

- Inno Setup
- Wix tools

### Générer l'exécutable

Il suffit d'exécuter le plugin javapackager via maven afin de générer un exécutable .deb ou .exe, en fonction du système sur lequelle est exécuter cette commande.

Pour plus d'information sur javapackager, cliquez [ici](https://github.com/fvarrui/JavaPackager)
