module com.example.institutmonteclair.epubadapter {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.web;

    requires org.controlsfx.controls;
    requires org.kordamp.ikonli.javafx;
    requires org.kordamp.bootstrapfx.core;
    requires jdk.jsobject;
    requires java.desktop;
    requires java.scripting;
    requires jdk.accessibility;
    requires javatuples;
    requires jdom;
    requires jaxen;
    requires org.jsoup;
    requires com.jfoenix;
    requires javafx.graphics;
    opens com.example.institutmonteclair.epubadapter to javafx.fxml;
    exports com.example.institutmonteclair.epubadapter;
}