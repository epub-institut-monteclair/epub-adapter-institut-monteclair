package com.example.institutmonteclair.epubadapter;

import javafx.util.Pair;

import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;

public class Fonts {

    private Path path;
    private ArrayList<Pair<String, String>> fonts;

    public void addFont(File file){
        String filename = file.getName();
        Path copied = Paths.get(this.path.toString(), filename);
        Path originalPath = Paths.get(file.getPath());
        try {
            Files.copy(originalPath, copied, StandardCopyOption.REPLACE_EXISTING);
            Font fo = Font.createFont(Font.TRUETYPE_FONT, new FileInputStream(copied.toString()));

            this.fonts.add(new Pair<>(fo.getFamily(), copied.toString()));
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public Fonts(Path path){
        this.path = path;
        this.fonts = new ArrayList<>();
        File[] files = this.path.toFile().listFiles();
        for(File f : files){
            try {
                Font fo = Font.createFont(Font.TRUETYPE_FONT, new FileInputStream(f.getPath()));

                this.fonts.add(new Pair<>(fo.getFamily(), f.getPath()));
            } catch (FontFormatException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    public ArrayList<Pair<String, String>> getFonts(){
        return this.fonts;
    }

    public String getFontPath(String name) throws Exception {
        for(Pair<String, String> font: this.fonts){
            if(font.getKey().equals(name)){
                return font.getValue();
            }
        }
        throw new Exception("Font not Found");
    }
}
