package com.example.institutmonteclair.epubadapter;

import com.jfoenix.controls.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.css.PseudoClass;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.web.HTMLEditor;
import javafx.scene.web.WebView;
import javafx.stage.*;
import javafx.stage.Popup;
import javafx.util.Callback;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Pair;

import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class OptionViewController extends GridPane
{
    @FXML
    private MenuItem btn_font;
    @FXML
    private Button btn_editor;
    @FXML
    private MenuItem btn_save;


    @FXML
    private MenuItem btn_ouvrir;
    @FXML
    private MenuItem btn_save_style;
    @FXML
    private MenuItem btn_save_under;

    @FXML
    private MenuItem btn_aide;

    @FXML
    private Menu btn_history;

    @FXML
    private GridPane grid_All_Elements;

    @FXML
    private GridPane grid_All_Element;

    @FXML
    private Label label_selection;

    private ToggleGroup rBtn_element_group;
    private JFXRadioButton rBtn_all;
    private JFXRadioButton rBtn_this;

    // Slider et TextField gérant la taille du texte
    @FXML
    private JFXSlider slider_font;
    @FXML
    private MenuItem browse;

    // Menus gérant respectivement la couleur du texte et la couleur du fond
    @FXML
    private ColorPicker text_color;
    @FXML
    private ColorPicker backgr_color;

    // Boutons "toggle" gérant respectivement Gras, Italique, Souligne
    @FXML
    private ToggleButton tglBtn_bold;
    @FXML
    private ToggleButton tglBtn_italique;
    @FXML
    private ToggleButton tglBtn_souligne;

    // Menu déroulant ayant les liste de police
    @FXML
    private JFXComboBox<ComboBoxItem> cb_police;

    // Groupe de boutonsRadio masquer/montrer élément
    @FXML
    private ToggleGroup rBtn_display_group;
    @FXML
    private JFXRadioButton rBtn_hide;
    @FXML
    private JFXRadioButton rBtn_show;

    // Groupe de boutons Alignement du texte
    @FXML
    private ToggleButton tglBtn_align_left;
    @FXML
    private ToggleButton tglBtn_align_center;
    @FXML
    private ToggleButton tglBtn_align_right;

    // Slider et TextField gérant l'espacement des caractères
    @FXML
    private JFXSlider slider_space;


    // Bouton qui permet de remplacer une image par un texte
    @FXML
    private JFXCheckBox checkBox_hide_image ;
    @FXML
    private JFXButton btn_change_text ;

    @FXML
    private Label label_image;
    private boolean is_saved;
    private MainViewController main;

    // Booleen qui permet de ne pas modifier l'historique du style quand l'interface est modifier par le clique sur la webview
    private boolean notdefault;

    final PseudoClass header = PseudoClass.getPseudoClass("section-header");

    public MenuItem getBtn_save() {
        return btn_save;
    }

    private final ChangeListener cb_police_listener = new ChangeListener()
    {
        @Override
        public void changed(ObservableValue observableValue, Object o, Object t1)
        {
            if(t1 == null)
                return;

            ComboBoxItem police = (ComboBoxItem) t1;


            if(!police.isInEpub()){

                try {
                    String fontPath  = main.getFonts().getFontPath(police.getName());
                    main.getEpub().addFont(Paths.get(fontPath));
                    main.webViewController.addFontDynamically(police.getName(), fontPath);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                main.getWebViewController().change_CSSRule("fontFamily", police.getName().replace("\"", ""));

                main.getWebViewController().addStyleSheets("Police du texte", police.getName(), "fontFamily", police.getName().replace("\"", ""), true);
                main.getWebViewController().reload();
                cb_police.getSelectionModel().selectedItemProperty().removeListener(cb_police_listener);
                build_cb_police();
                defaultPolice(police.getName());
                cb_police.getSelectionModel().selectedItemProperty().addListener(cb_police_listener);



            }else if(o == null)
                return;
            else if(!((ComboBoxItem) o).getName().equals(((ComboBoxItem)t1).getName())) {
                main.getWebViewController().change_CSSRule("fontFamily", police.getName().replace("\"", ""));

                main.getWebViewController().addStyleSheets("Police du texte", police.getName(), "fontFamily", police.getName().replace("\"", ""), true);

            }

        }
    };
    public void setIs_saved(boolean is_saved){
        this.is_saved = is_saved;
    }
    public MenuItem getBrowse() {
        return browse;
    }

    /**
     * @author Julien Laffely
     * Constructeur de l'OptionViewController : il permet d'avoir un element fxml custom
     */
    public OptionViewController(){
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("Options_view.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        is_saved = true;
        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    /**
     * @author Julien Laffely et Guillaume GRENON
     *
     * Initialisation de l'interface utilisateur
     * Tous les évènements liés à l'interface utilisateur
     */
    @FXML
    public void initialize() throws IOException, FontFormatException {
        this.notdefault = true;


        // Paramètres Slider taille texte
        this.slider_font.setMin(0);
        this.slider_font.setMax(50);
        this.slider_font.setShowTickMarks(true);
        this.slider_font.setShowTickLabels(true);
        this.slider_font.setMajorTickUnit(50);
        this.slider_font.setBlockIncrement(10);
        this.slider_font.setValue(0);
        // Paramètres TextField taille texte
        // Déclaration du ToggleGroup permettant de faire en sorte qu'un seul bouton ne peut être activé à la fois, parmis tout ceux du groupe
        // Groupe de Boutons "toggle" gérant les alignements du texte
        ToggleGroup tglGroup_text_alignement = new ToggleGroup();
        // On attribue ce ToggleGroup aux boutons concernant l'alignement du texte (un seul alignement de texte possible à la fois)
        this.tglBtn_align_left.setToggleGroup(tglGroup_text_alignement);
        this.tglBtn_align_center.setToggleGroup(tglGroup_text_alignement);
        this.tglBtn_align_right.setToggleGroup(tglGroup_text_alignement);

        // Paramètres Slider espacement texte
        this.slider_space.setMin(0);
        this.slider_space.setMax(200);
        this.slider_space.setShowTickMarks(true);
        this.slider_space.setShowTickLabels(true);
        this.slider_space.setMajorTickUnit(50);
        this.slider_space.setBlockIncrement(10);
        this.slider_space.setValue(0);
        // Paramètres TextField espacement texte

        // Récupère la liste des polices de texte présentent sur l'ordinateur, et les stocks dans un tableau
        GraphicsEnvironment gE = GraphicsEnvironment.getLocalGraphicsEnvironment();

        /*
        // Prend le chemin des fichiers de font de l'epub
        File dir = new File("XXX direction du dossier XXX");
        // Vérifie que les fichiers sont des fonts (.ttf format de font)
        FileFilter fileFilter = file -> !file.isDirectory() && file.getName().endsWith(".ttf");

        // On vérifie que le dossier a des fichier .ttf
        if (dir.listFiles(fileFilter).length > 0)
        {
            // Crée une liste de fichier terminant pas .ttf dans le dossier de fonts
            File[] allFiles = dir.listFiles(fileFilter);

            // Pour tout ces fichiers, on les ajoute dans l'environnement graphique
            for (File f : allFiles) {
                gE.registerFont(Font.createFont(Font.TRUETYPE_FONT, getClass().getResourceAsStream(f.getAbsolutePath())));
            }
        }
        */

        cb_police.setCellFactory(listView -> new ListCell<ComboBoxItem>() {
            @Override
            public void updateItem(ComboBoxItem item, boolean empty){
                super.updateItem(item, empty);
                if (empty) {
                    setText(null);
                    setDisable(false);
                    pseudoClassStateChanged(header, false);
                } else {
                    setText(item.toString());
                    setDisable(! item.isSelectable());
                    pseudoClassStateChanged(header, ! item.isSelectable());
                }
            }
        });
        // Boutons en tête ----------------------------------------------------------------------------------------------------------------------------------------------------------------------

        this.browse.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent actionEvent)
            {
                FileChooser fileChooser = new FileChooser();
                fileChooser.setTitle("Ouverture Epub");
                fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("EPUB","*.epub"));
                File file = fileChooser.showOpenDialog(main.getStage());


                if (file!=null && main.ouverture_EPUB(file))
                {
                    grid_All_Elements.setDisable(false);
                    build_cb_police();
                }




            }
        });

        // Bouton de sauvegarde -----------------------------------------------------------------------------------------------------------------------------------
        this.btn_save.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent actionEvent)
            {

                main.getWebViewController().save();
                try
                {
                    String epub_path = main.getEpub().getPath();
                    main.getEpub().zipEpub(Paths.get(epub_path), main.savemanager.getEpubWorkspace());
                    is_saved = true;
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        });

        this.btn_font.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                FileChooser fileChooser = new FileChooser();
                fileChooser.setTitle("Import police");
                fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter(".ttf","*.ttf"));
                List<File> files = fileChooser.showOpenMultipleDialog(main.getStage());
                for (File f: files) {
                    if(f.getName().endsWith(".ttf")){
                        main.getFonts().addFont(f);
                    }
                }
                build_cb_police();
            }
        });


        this.btn_aide.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                try {
                    File  htmlFile = SaveManager.getInstance().getHelp().toFile();
                    Desktop.getDesktop().browse(htmlFile.toURI());

                } catch (IOException e) {
                    e.printStackTrace();
                }
            };
        });
        // Bouton enregistrer sous --------------------------------------------------------------------------------------------------------------------------------
        this.btn_save_under.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent actionEvent)
            {
                main.getWebViewController().save();
                FileChooser fileChooser = new FileChooser();
                // Ouvre la boite de dialogue dans répertoire courant
                fileChooser.setInitialDirectory(new File("."));
                // Nom de la boite de dialogue
                fileChooser.setTitle("Enregistrer sous");

                FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Epub File (.epub)", "*.epub");
                fileChooser.getExtensionFilters().add(extFilter);

                File file = fileChooser.showSaveDialog(main.getStage());

                if (file != null)
                {
                    // Chemin absolu
                    String chemin = file.getAbsolutePath();

                    try
                    {
                        main.getEpub().zipEpub(Paths.get(chemin), main.savemanager.getEpubWorkspace());
                        is_saved = true;
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }

                }
            }
        });

        // Bouton enregistrer sous de la feuille de style --------------------------------------------------------------------------------------------------------------------------------
        this.btn_save_style.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent actionEvent)
            {
                main.getWebViewController().save();
                FileChooser fileChooser = new FileChooser();
                // Ouvre la boite de dialogue dans répertoire courant
                fileChooser.setInitialDirectory(new File("."));
                // Nom de la boite de dialogue
                fileChooser.setTitle("Enregistrer sous");

                FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("CSS File (.css)", "*.css");
                fileChooser.getExtensionFilters().add(extFilter);
                fileChooser.setSelectedExtensionFilter(extFilter);

                File dest= fileChooser.showSaveDialog(main.getStage());

                if (dest != null)
                {
                    // Chemin absolu
                    Path chemin_source = Paths.get(main.getEpub().getEpubRootFolder().toString(), "CStyles", "custom_global.css");
                    Path chemin_dest = dest.toPath();

                    try
                    {
                        File fichier_existant = new File(chemin_dest.toString());
                        if(!fichier_existant.exists())  // On verifie qu'un fichier du meme nom n'existe pas déjà
                            Files.copy(chemin_source,chemin_dest);
                        else{
                            // Creation d'une nouvelle fenetre de dialogue
                            final Stage dialog = new Stage();
                            dialog.setResizable(false);
                            dialog.setTitle("Fichier déjà existant !");
                            dialog.initModality(Modality.APPLICATION_MODAL);
                            dialog.initOwner(main.getStage());
                            HBox dialogHbox = new HBox(15);

                            Button btn_ecraser = new Button("Ecraser");
                            Button btn_annuler = new Button("Annuler");

                            // On detruit le fichier existant et on en crée un nouveau
                            btn_ecraser.setOnAction(new EventHandler<ActionEvent>()
                            {
                                @Override
                                public void handle(ActionEvent actionEvent)
                                {
                                    fichier_existant.delete();
                                    try {
                                        Files.copy(chemin_source,chemin_dest);
                                        dialog.close();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });

                            btn_annuler.setOnAction(new EventHandler<ActionEvent>()
                            {
                                @Override
                                public void handle(ActionEvent actionEvent)
                                {
                                    dialog.close();
                                }
                            });

                            dialogHbox.getChildren().add(btn_ecraser);
                            dialogHbox.getChildren().add(btn_annuler);
                            Scene dialogScene = new Scene(dialogHbox, 300, 100);
                            dialog.setScene(dialogScene);
                            dialog.show();
                        }

                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }

                }
            }
        });
        this.btn_ouvrir.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent actionEvent)
            {
                FileChooser fileChooser = new FileChooser();
                fileChooser.setTitle("Ouverture de fichier de style");
                fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("CSS", "*.css"));
                File file = fileChooser.showOpenDialog(main.getStage());

                if (file != null)
                {
                    try
                    {

                        main.getWebViewController().importCss(file.toPath());

                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
            }

        });

        // Interface corps ----------------------------------------------------------------------------------------------------------------------------------------------------------------------


        // Taille du texte (Slider et TextField) ------------------------------------------------------------------------------------------------------------------


        this.slider_font.valueProperty().addListener(new ChangeListener<Number>() {
            public void changed(ObservableValue<? extends Number> ov,
                                Number old_val, Number new_val)
            {
                double val = Math.floor((double)new_val * 10) / 10;
                slider_font.setValue(val);

                main.getWebViewController().change_CSSRule("fontSize",String.valueOf(val)+"pt");

            }
        });


        this.slider_font.valueChangingProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(
                    ObservableValue<? extends Boolean> observableValue,
                    Boolean wasChanging,
                    Boolean changing) {

                if (!changing && notdefault) {
                    main.getWebViewController().addStyleSheets("Taille du texte",String.valueOf(slider_font.getValue()),"fontSize",String.valueOf(slider_font.getValue())+"pt",true);
                }
            }
        });

        // Couleur texte ---------------------------------------------------------------------------------------------------------------------------------------

        // Evenement : Quand dans l'optionView, la couleur est modifiée, la couleur du texte dans la webview prend cette nouvelle couleur
        this.text_color.valueProperty().addListener(new ChangeListener<Color>()
        {
            @Override
            public void changed(ObservableValue<? extends Color> observableValue, Color color, Color t1)
            {
                text_color.setValue(t1);
                String new_Color = String.valueOf(t1).replace("0x", "#");

                main.getWebViewController().change_CSSRule("color", new_Color);
                if(notdefault)
                    main.getWebViewController().addStyleSheets("Couleur du texte",new_Color,"color", new_Color,true);
            }
        });

        // Couleur fond ---------------------------------------------------------------------------------------------------------------------------------------


        // Évènement : Quand dans l'optionView, la couleur du fond est modifiée, la couleur du fond dans la webview prend cette nouvelle couleur
        this.backgr_color.valueProperty().addListener(new ChangeListener<Color>()
        {
            @Override
            public void changed(ObservableValue<? extends Color> observableValue, Color color, Color t1)
            {
                backgr_color.setValue(t1);
                String new_Color = String.valueOf(t1).replace("0x", "#");

                main.getWebViewController().change_CSSRule("background", new_Color);
                if(notdefault)
                    main.getWebViewController().addStyleSheets("Couleur de fond",new_Color,"background", new_Color,true);
            }
        });

        // Boutons : Gras, Italique, Souligne ------------------------------------------------------------------------------------------------------------------

        // On ne met pas ces boutons dans ToggleGroup car on veut pouvoir les avoir activés en meme temps ce qui va à l'encontre du principe même des toggleGroup

        // Évènement : bouton Gras activer : applique le CSS correspondant, désactiver, enlève le css correspondant
        this.tglBtn_bold.selectedProperty().addListener(new ChangeListener<Boolean>()
        {
            @Override
            public void changed(ObservableValue<? extends Boolean> observableValue, Boolean aBoolean, Boolean t1)
            {
                if(tglBtn_bold.isSelected())
                {
                    main.getWebViewController().change_CSSRule("fontWeight", "bold");
                    if(notdefault)
                        main.getWebViewController().addStyleSheets("gras","activé","fontWeight", "bold",true);
                }
                else
                {
                    main.getWebViewController().change_CSSRule("fontWeight", "normal");
                    if(notdefault)
                        main.getWebViewController().addStyleSheets("Gras","désactivé","fontWeight", "normal",true);
                }
            }
        });

        // Évènement : bouton Italique activer : applique le CSS correspondant, désactiver, enlève le css correspondant
        this.tglBtn_italique.selectedProperty().addListener(new ChangeListener<Boolean>()
        {
            @Override
            public void changed(ObservableValue<? extends Boolean> observableValue, Boolean aBoolean, Boolean t1)
            {
                if(tglBtn_italique.isSelected())
                {
                    main.getWebViewController().change_CSSRule("fontStyle", "italic");
                    if(notdefault)
                        main.getWebViewController().addStyleSheets("Italique","activé","fontStyle", "italic",true);
                }
                else
                {
                    main.getWebViewController().change_CSSRule("fontStyle", "normal");
                    if(notdefault)
                        main.getWebViewController().addStyleSheets("Italique","désactivé","fontStyle", "normal",true);
                }
            }
        });

        // Évènement : bouton Souligner activer : applique le CSS correspondant, désactiver, enlève le css correspondant
        this.tglBtn_souligne.selectedProperty().addListener(new ChangeListener<Boolean>()
        {
            @Override
            public void changed(ObservableValue<? extends Boolean> observableValue, Boolean aBoolean, Boolean t1)
            {
                if(tglBtn_souligne.isSelected())
                {
                    main.getWebViewController().change_CSSRule("textDecoration", "underline");
                    main.getWebViewController().addStyleSheets("Souligne","activé","textDecoration", "underline",true);
                }
                else
                {
                    main.getWebViewController().change_CSSRule("textDecoration", "none");
                    if(notdefault)
                        main.getWebViewController().addStyleSheets("Souligne","desactivé","textDecoration", "none",true);
                }
            }
        });

        // Menu déroulant Police ------------------------------------------------------------------------------------------------------------------------------

        cb_police.getSelectionModel().selectedItemProperty().addListener(cb_police_listener);


        // Groupe de boutons masquer/montrer l'élément --------------------------------------------------------------------------------------------------------

        this.rBtn_display_group.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> observableValue, Toggle toggle, Toggle t1)
            {
                if(rBtn_hide.isSelected())
                {
                    main.getWebViewController().change_CSSRule("opacity", "0");
                    if(notdefault)
                        main.getWebViewController().addStyleSheets("Opacité","0", "opacity", "0", true);
                }
                else
                {
                    main.getWebViewController().change_CSSRule("opacity", "100");
                    if(notdefault)
                        main.getWebViewController().addStyleSheets("Opacité","100", "opacity", "100", true);
                }
            }
        });

        // Checkbox disparaitre -------------------------------------------------------------------------------------------------------------------------------

       /*this.checkb_hide.selectedProperty().addListener(new ChangeListener<Boolean>()
        {
            @Override
            public void changed(ObservableValue<? extends Boolean> observableValue, Boolean aBoolean, Boolean t1)
            {
                if(checkb_hide.isSelected())
                {
                    main.getWebViewController().change_CSSRule("opacity", "0");
                    if(notdefault)
                        main.getWebViewController().addStyleSheets("Opacité","0","opacity", "0",true);
                }
                else
                {
                    main.getWebViewController().change_CSSRule("opacity", "100");
                    if(notdefault)
                        main.getWebViewController().addStyleSheets("Opacité","100","opacity", "100",true);
                }
            }
        });*/

        // Alignement du texte (gauche, centre, droite, justifier) --------------------------------------------------------------------------------------------

        // Ici on a besoin d'un Toggle group car il n'est pas possible d'avoir plusieurs alignements de texte en même temps, donc logique de ne pouvoir activer qu'un seul bouton à la fois

        tglGroup_text_alignement.selectedToggleProperty().addListener(new ChangeListener<Toggle>()
        {
            @Override
            public void changed(ObservableValue<? extends Toggle> observableValue, Toggle toggle, Toggle t1)
            {
                if(tglBtn_align_left.isSelected())
                {
                    main.getWebViewController().change_CSSRule("textAlign", "start");
                    if(notdefault)
                        main.getWebViewController().addStyleSheets("Alignement du texte","GAUCHE","textAlign", "start",true);
                }
                else if(tglBtn_align_center.isSelected())
                {
                    main.getWebViewController().change_CSSRule("textAlign", "center");
                    if(notdefault)
                        main.getWebViewController().addStyleSheets("Alignement du texte","CENTRER","textAlign", "center",true);
                }
                else if(tglBtn_align_right.isSelected())
                {
                    main.getWebViewController().change_CSSRule("textAlign", "end");
                    if(notdefault)
                        main.getWebViewController().addStyleSheets("Alignement du texte","DROITE","textAlign", "end",true);
                }

            }
        });

        // Espacement des caractères (Slider et TextField) --------------------------------------------------------------------------------------------------------

        this.slider_space.valueProperty().addListener((observableValue, number, t1) ->
        {
            double space = Math.floor((double)t1 * 10) / 10;
            slider_space.setValue(space);

            // Change l'espacement entre chaque PARAGRAPHE
            main.getWebViewController().change_CSSRule("padding",String.valueOf(space)+"pt 0pt");

            // Change l'espacement entre chaque CARACTÈRES
            //main.getWebViewController().change_CSSRule("letterSpacing", space +"pt");

        });

        this.slider_space.valueChangingProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(
                    ObservableValue<? extends Boolean> observableValue,
                    Boolean wasChanging,
                    Boolean changing) {

                if (!changing && notdefault) {
                    main.getWebViewController().addStyleSheets("Espacement",String.valueOf(slider_space.getValue()),"padding",String.valueOf(slider_space.getValue())+"pt 0pt",true);
                }
            }
        });

        // Evenement sur le bouton de remplacement d'une image par du texte -> si non coché alors desactive toutes les options de modification

        this.checkBox_hide_image.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if(checkBox_hide_image.isSelected()){
                    // On active la modification de style du texte
                    enableAll();

                    open_dialog_change_text(true);
                }
                else{
                    disableAll();
                    main.getWebViewController().setImage();
                }
            }
        });

        // Evenement sur le bouton qui permet de modifier le texte de remplacement d'une image qui est déjà remplacer par du texte

        this.btn_change_text.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                //main.getWebViewController().setImage();
                open_dialog_change_text(false);
            }
        });

    }

    private void build_cb_police(){
        cb_police.getItems().clear();
        ArrayList<String> epubFonts = main.getEpub().getFonts();
        ArrayList<Pair<String, String>> appFonts = main.getFonts().getFonts();
        ArrayList<ComboBoxItem> l = new ArrayList<ComboBoxItem>();

        if(epubFonts.size()>0){
            l.add(new ComboBoxItem("Polices incluses dans l'epub", false, false));

            for(int i = 0; i< epubFonts.size(); i++){
                l.add(new ComboBoxItem(epubFonts.get(i), true, true));
            }
        }


        l.add(new ComboBoxItem("Polices disponibles", false, false));

        for(Pair<String, String> font : appFonts){
            if(!l.stream().filter(o -> o.getName().equals(font.getKey())).findFirst().isPresent())
                l.add(new ComboBoxItem(font.getKey(), true, false));
        }

        cb_police.getItems().addAll(l);
        System.out.println(cb_police.getItems().size());
    }


    //  -----------------------------------------------------------------------------------------------------------------------------------------------------------


    /**
     * @author Julien Laffely
     * Ajout de l'attribut MainController
     */
    public void init(MainViewController main){
        this.main=main;
    }

    // Élément sélectionné


    public void defaultOpacity(boolean hidden){
        if(hidden)
            this.rBtn_hide.setSelected(true);
        else
            this.rBtn_show.setSelected(true);
    }
    public boolean getIsSaved() {
        return is_saved;
    }

    /**
     * @author Guillaume GRENON
     * @param element
     * Indique quel élément est sélectionné lors d'un clique en convertissant le nom de la balise html en ce à quoi cela correspond ( p devient paragraphes )
     */
    public void defaultLabel(String element)
    {
        this.notdefault =false;
        switch (element)
        {
            // si balise p est rencontré
            case "data" :
                label_selection.setText("Remplacement d'image");
                break;
            case "p" :
                label_selection.setText("Paragraphe");
                break;

            case "h1" :
                label_selection.setText("Titre 1");
                break;
            case "h2" :
                label_selection.setText("Titre 2");
                break;
            case "h3" :
                label_selection.setText("Titre 3");
                break;
            case "h4" :
                label_selection.setText("Titre 4");
                break;
            case "h5" :
                label_selection.setText("Titre 5");
                break;
            case "h6" :
                label_selection.setText("Titre 6");
                break;

            case "img" :
                label_selection.setText("Image");
                break;


            default :
                label_selection.setText(element);
                break;
        }
        this.notdefault =true;


    }

    // Taille texte

    /**
     * @author Julien Laffely
     * @param value
     * Place le curseur sur le slider du fontSize En fonction de l'element cliquer
     */
    public void defaultSlider(double value)
    {
        this.notdefault =false;
        double val = Math.floor(value * 10) / 10;
        slider_font.setValue(val);
        this.notdefault =true;
    }

    // Styles de caractère

    /**
     * @author Guillaume GRENON
     * @param bold
     * Bouton Gras mis à jour dans l'optionView lors d'un clic sur un élément
     */
    public void defaultBold(String bold)
    {
        this.notdefault =false;
        tglBtn_bold.setSelected((Objects.equals(bold, "bold")) || (Objects.equals(bold, "bolder")) || (Objects.equals(bold, "700")));
        this.notdefault =true;
    }

    /**
     * @author Guillaume GRENON
     * @param italic
     * Bouton Italique mis à jour dans l'optionView lors d'un clic sur un élément
     */
    public void defaultItalic(String italic)
    {
        this.notdefault =false;
        tglBtn_italique.setSelected(Objects.equals(italic, "italic"));
        this.notdefault =true;
    }

    /**
     * @author Guillaume GRENON
     * @param souligne
     * Bouton soulignement mis à jour dans l'optionView lors d'un clic sur un élément
     */
    public void defaultSouligne(String souligne)
    {
        this.notdefault =false;
        tglBtn_souligne.setSelected(Objects.equals(souligne, "underline"));
        this.notdefault =true;
    }

    // Police de caractère

    /**
     * @author Guillaume GRENON
     * @param police
     * Police de caractères mis à jour dans l'optionView lors d'un clic sur un élément
     */
    public void defaultPolice(String police)
    {
        this.notdefault =false;

        String[] listFonts = police.split(",");

        if(cb_police.getValue() != null){
            for(int i = listFonts.length-1; i>=0;i--){
                if(cb_police.getValue().getName().equals(listFonts[i])){
                    this.notdefault =false;
                    return;
                }
            }
        }
        for(int i = listFonts.length-1; i>=0;i--){
            String finalFontName = listFonts[i].strip();
            for(ComboBoxItem item:cb_police.getItems()){
                if(item.getName().equals(finalFontName)){
                    cb_police.setValue(item);

                    break;

                }
            }
        }

        this.notdefault =true;
    }

    // Couleur de caractère

    /**
     * @author Guillaume GRENON
     * @param color
     * Couleur de texte mis à jour dans l'optionView lors d'un clic sur un élément
     */
    public void defaultColor(String color)
    {
        this.notdefault =false;
        text_color.setValue(Color.valueOf(color));
        this.notdefault =true;
    }

    // Couleur de fond

    /**
     * @author Guillaume GRENON
     * @param backgroundColor
     * Couleur de fond mis à jour dans l'optionView lors d'un clic sur un élément
     */
    public void defaultBackgroundColor(String backgroundColor)
    {
        this.notdefault =false;
        backgr_color.setValue(Color.valueOf(backgroundColor));
        this.notdefault =true;
    }

    // Alignement de texte

    /**
     * @author Guillaume GRENON
     * @param alignement
     * Alignement du texte mis à jour dans l'optionView lors d'un clic sur un élément
     */
    public void defaultAlignement(String alignement)
    {
        this.notdefault =false;
        if (Objects.equals(alignement, "center"))
        {
            tglBtn_align_center.setSelected(true);
        }
        else if ((Objects.equals(alignement, "right")) || (Objects.equals(alignement, "end")))
        {
            tglBtn_align_right.setSelected(true);
        }
        // S'il n'est pas spécifié d'attributs pour l'alignement du text, il est automatiquement mis considéré comme en left
        else
        {
            tglBtn_align_left.setSelected(true);
        }
        this.notdefault =true;
    }

    // Espacement

    /**
     * @author Guillaume GRENON
     * @param space
     * Slider et TextView espacement mis à jour dans l'optionView lors d'un clic sur un élément
     */
    public void defaultSliderSpace(double space) {
        this.notdefault =false;
        double getSpace = Math.floor(space * 10) / 10;
        if (Double.isNaN(getSpace)) {
            getSpace = 0;
        }
        slider_space.setValue(getSpace);
        this.notdefault =true;
    }

    /**
     * @author Julien Laffely
     * Place le curseur à 0 et vide le champ de texte avec la taille du font
     */
    public void resetSlider(){
        this.notdefault =false;
        this.slider_font.setValue(0);
        this.notdefault =true;
    }

    /**
     * @author Julien Laffely
     * Modifie l'hisorique de l'interface utilisateur en fonction des modifications sur livre
     */
    public void updateHistory(ArrayList<String> history){
        // On vide l'historique et on l'actualise en fonction des modifications qu'il y a eu
        this.btn_history.getItems().clear();
        for(String s : history) {
            CustomMenuItem item = new CustomMenuItem(new CheckBox(s));
            ((CheckBox)item.getContent()).setSelected(true);
            item.setHideOnClick(false);
            ((CheckBox)item.getContent()).setStyle("-fx-text-fill:black !important");
            item.setStyle("-fx-text-fill:black");
            this.btn_history.getItems().add(item);
            item.setId(String.valueOf(btn_history.getItems().size()-1));
            item.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    for(int i = 0; i <btn_history.getItems().size() ; ++i) {
                        if (i <= Integer.parseInt(item.getId()))
                            ((CheckBox)((CustomMenuItem) btn_history.getItems().get(i)).getContent()).setSelected(true);
                        else {
                            if (i < btn_history.getItems().size() - 2)
                                ((CheckBox)((CustomMenuItem) btn_history.getItems().get(i)).getContent()).setSelected(false);
                        }
                    }
                }
            });
        }
        // Creation d'un separateur avec le bouton appliquer
        SeparatorMenuItem separator = new SeparatorMenuItem();
        this.btn_history.getItems().add(separator);
        MenuItem appliquer = new MenuItem("Appliquer");
     //   appliquer.setStyle("-fx-background-color:cyan");
        this.btn_history.getItems().add(appliquer);

        // Lorsqu'on appuie sur le bouton applique cela applique le dernier style selectionner
        appliquer.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                boolean found = false;
                for(int i = 0 ; i < btn_history.getItems().size();++i)
                    if(btn_history.getItems().get(i).getClass()==CustomMenuItem.class) {
                        if (!(((CheckBox)((CustomMenuItem) btn_history.getItems().get(i)).getContent()).isSelected()) && !found) {
                            main.getWebViewController().selectionStyleSheets(Integer.parseInt(btn_history.getItems().get(i - 1).getId()));
                            found = true;
                        }
                    }

                if(!found)
                    main.getWebViewController().selectionStyleSheets(Integer.parseInt(btn_history.getItems().get(btn_history.getItems().size()-3).getId()));
            }
        });
    }


    /**
     * @author Julien Laffely
     * Retourne l'historique des modifications
     */
    public Menu getBtn_history(){
        return this.btn_history;
    }

    /**
     * @author Julien Laffely
     * Désactive toutes les options de modifications de l'interface utilisateur
     */
    public void disableAll(){
        this.slider_font.setDisable(true);
        this.backgr_color.setDisable(true);
        this.cb_police.setDisable(true);
        this.slider_space.setDisable(true);
        this.text_color.setDisable(true);
        this.tglBtn_align_center.setDisable(true);
        this.tglBtn_align_left.setDisable(true);
        this.tglBtn_bold.setDisable(true);
        this.tglBtn_italique.setDisable(true);
        this.tglBtn_souligne.setDisable(true);
        this.tglBtn_align_right.setDisable(true);
        this.rBtn_hide.setDisable(true);
        this.rBtn_show.setDisable(true);
    }

    /**
     * @author Julien Laffely
     * Active toutes les options de modifications de l'interface utilisateur
     */
    public void enableAll(){
        this.slider_font.setDisable(false);
        this.backgr_color.setDisable(false);
        this.cb_police.setDisable(false);
        this.slider_space.setDisable(false);
        this.text_color.setDisable(false);
        this.tglBtn_align_center.setDisable(false);
        this.tglBtn_align_left.setDisable(false);
        this.tglBtn_bold.setDisable(false);
        this.tglBtn_italique.setDisable(false);
        this.tglBtn_souligne.setDisable(false);
        this.tglBtn_align_right.setDisable(false);
        this.rBtn_hide.setDisable(false);
        this.rBtn_show.setDisable(false);
    }

    /**
     * @author Julien Laffely
     * @param visible Definie si on affiche ou non la checkbox
     * Rend visible ou invisible la checkbox permettant de remplacer une image et d'activer ou desactiver
     * toutes les options de l'optionview en fonction de la valeur de la checkbox
     */
    public void imgSelected(Boolean visible){
        this.checkBox_hide_image.setVisible(visible);
        this.btn_change_text.setVisible(visible);
        this.label_image.setVisible(visible);

        if(!main.getWebViewController()._selectionTagName.startsWith("data") && visible) {
            disableAll();
        }
        else {
            enableAll();
        }
    }

    /**
     * @author Julien Laffely
     * @param check definie si la case est coché ou non
     * Coche la case de remplacement d'une image ou non
     */
    public void imgReplaced(Boolean check){
        this.checkBox_hide_image.setSelected(check);
    }


    /**
     * @author Julien Laffely
     * @param replaceImg permet de savoir si l'image selection est déjà replacer ou non
     * Ouvre la fenetre de dialogue pour modifier le texte de remplacement d'une image
     */
    public void open_dialog_change_text(boolean replaceImg){
        // Creation d'une nouvelle fenetre de dialogue
        final Stage dialog = new Stage();
        dialog.setResizable(false);
        dialog.setTitle("Remplacement de l'image");
        dialog.initModality(Modality.APPLICATION_MODAL);
        dialog.initOwner(main.getStage());
        VBox dialogVbox = new VBox(15);

        Label lblTextRemplacement = new Label("Remplacer l'image selectionner par :");
        TextField altImage = new TextField(main.getWebViewController().getAltImage());
        Button btn_replace = new Button("Appliquer");

        // On ecrit dans l'attribut alt de l'image
        btn_replace.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if (!altImage.getText().isEmpty()){
                    main.getWebViewController().replaceImageBy(altImage.getText(),replaceImg);
                    dialog.close();

                }
                else {
                    checkBox_hide_image.setSelected(false);
                    btn_change_text.setVisible(false);
                    disableAll();
                    dialog.close();
                }
            }
        });

        // Lorsque la dialogue est fermé par la croix alors on décoche la case
        dialog.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent windowEvent) {
                checkBox_hide_image.setSelected(false);
                btn_change_text.setVisible(false);
                disableAll();
            }
        });


        dialogVbox.getChildren().add(lblTextRemplacement);
        dialogVbox.getChildren().add(altImage);
        dialogVbox.getChildren().add(btn_replace);
        Scene dialogScene = new Scene(dialogVbox, 300, 100);
        dialog.setScene(dialogScene);
        dialog.show();
    }

    /**
     * @author GRENON Guillaume
     * Crée la fenêtre ayant un HTMLEditor, qui permet de modifier le texte html de l'élément cliqué, sans voir les balises avec le bouton enregistrer qui applique à la webView les modifications effectuées
     */
    public void open_HTMLEditor()
    {
        // Creation d'une nouvelle fenêtre
        final Stage stage = new Stage();
        stage.setResizable(false);
        stage.setTitle("Editeur de texte");
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initOwner(main.getStage());

        // Création de conteneur vertical
        VBox root = new VBox();

        // Création de l'éditeur de texte/html
        HTMLEditor html_editor = new HTMLEditor();
        String html_text = main.getWebViewController().get_selection_element();

        html_editor.setHtmlText(html_text);

        Button btn_save = new Button("Enregistrer");

        // Evènement bouton, quand on clique dessus, applique les modifications et ferme la fenêtre
        btn_save.setOnMouseClicked(new EventHandler<MouseEvent>()
        {
            @Override
            public void handle(MouseEvent mouseEvent)
            {
                // Appel la fonction set_inner_html qui modifie le texte réalisé dans l'HTMLEditor et l'applique à la webview
                main.getWebViewController().set_inner_HTML(html_editor.getHtmlText());
                stage.close();
            }
        });

        // Lorsque la fenêtre est fermée
        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent windowEvent) {
                disableAll();
            }
        });

        // Ajoute au conteneur fxml l'HTMLEditor et le bouton
        root.getChildren().addAll(html_editor,btn_save);

        // Applique ce conteneur à la scene puis l'affiche
        Scene dialogScene = new Scene(root, USE_COMPUTED_SIZE, USE_COMPUTED_SIZE);
        stage.setScene(dialogScene);
        stage.show();
    }
    public static class ComboBoxItem {
        private final String name ;
        private final boolean selectable ;
        private final boolean inEpub;

        public ComboBoxItem(String name, boolean selectable, boolean inEpub) {
            this.name = name ;
            this.selectable = selectable ;
            this.inEpub = inEpub;
        }

        public String getName() {
            return name ;
        }

        public boolean isSelectable() {
            return selectable ;
        }

        public boolean isInEpub() {
            return inEpub;
        }

        @Override
        public String toString() {
            return name ;
        }
    }





}














