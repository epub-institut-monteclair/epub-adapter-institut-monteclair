package com.example.institutmonteclair.epubadapter;

import javafx.util.Pair;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.*;
import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class Epub {

    private String path;
    private static String[] tags = new String[]{ "p", "h1", "h2", "h3", "h4", "h5", "h6", "em", "strong", "mark", "ul", "li", "ol", "pre", "code", "quote", "span", "img" };
    private Path workspace;

    private Path epubRootFolder;
    private Path epubRootFile;
    private HashMap<Integer, Pair<String, String>> pages;
    private ArrayList<String> fonts;

    public Epub(String path, Path workspace) throws Exception{
        this.path = path;
        this.workspace = workspace;
        this.pages = new HashMap<>();
        this.fonts = new ArrayList<>();
        this.cleanWorkspace();
        this.extractEpub(Paths.get(this.path), this.workspace);
        this.setEpubRootPath();
        this.setEpubPages();
        this.createCss();
        this.createRefsPage();
        this.setEpubFonts();
    }

    private void cleanWorkspace() throws IOException {
        Files.walk(this.workspace)
                .sorted(Comparator.reverseOrder())
                .map(Path::toFile)
                .forEach(File::delete);
    }

    private void setEpubRootPath() throws Exception {
        Path metaInfPath = Paths.get(this.workspace.toString(), "META-INF", "container.xml");
        File metaInfFile = new File(metaInfPath.toString());

        if (metaInfFile.exists()){
            Document containerDocument = buildDocument(metaInfFile);
            XPath xPath = XPathFactory.newInstance().newXPath();
            NodeList rootFiles = (NodeList) xPath.evaluate("/container/rootfiles/rootfile", containerDocument, XPathConstants.NODESET);

            if (rootFiles.getLength() == 1){

                this.epubRootFile = Paths.get(this.workspace.toString(), rootFiles.item(0).getAttributes().getNamedItem("full-path").getNodeValue());
                this.epubRootFolder = this.epubRootFile.getParent();

            } else if (rootFiles.getLength() == 0){
                throw new Exception("Pas de rootfile");
            } else {
                throw new Exception("Plusieurs rootfile");
            }
        } else {
            throw new Exception("Pas un epub");
        }
    }

    public String getPath() {
        return path;
    }

    private Path getEpubRootFile(){
        return this.epubRootFile;
    }

    private void setEpubPages() throws Exception {
        File rootFile = new File(this.epubRootFile.toString());

        if (rootFile.exists()){
            Document rootDocument = buildDocument(rootFile);
            XPath xPath = XPathFactory.newInstance().newXPath();
            NodeList itemsRef = (NodeList) xPath.evaluate("/package/spine/itemref", rootDocument, XPathConstants.NODESET);

            if (itemsRef.getLength() != 0){
                for(int i = 0; i < itemsRef.getLength(); i++){
                    String idRef = itemsRef.item(i).getAttributes().getNamedItem("idref").getNodeValue();
                    Node item = (Node) xPath.evaluate(String.format("/package/manifest/item[@id='%s']", idRef), rootDocument, XPathConstants.NODE);
                    Pair<String, String> page = new Pair<>(
                            item.getAttributes().getNamedItem("id").getNodeValue(),
                            Paths.get(this.epubRootFolder.toString(), item.getAttributes().getNamedItem("href").getNodeValue()).toString()
                    );

                    this.pages.put(i, page);
                }
            } else {
                throw new Exception("Epub vide");
            }
        } else {
            throw new Exception("Rootfile n'existe pas");
        }
    }

    private void setEpubFonts() throws Exception {
        File rootFile = new File(this.epubRootFile.toString());

        if (rootFile.exists()){
            Document rootDocument = buildDocument(rootFile);
            XPath xPath = XPathFactory.newInstance().newXPath();
            NodeList itemsRef = (NodeList) xPath.evaluate("/package/manifest/item", rootDocument, XPathConstants.NODESET);

            if (itemsRef.getLength() != 0){
                for(int i = 0; i < itemsRef.getLength(); i++){
                    String id = itemsRef.item(i).getAttributes().getNamedItem("href").getNodeValue();
                    if(id.endsWith(".css")) {
                        String content = Files.readString(Paths.get(this.epubRootFolder.toString(), itemsRef.item(i).getAttributes().getNamedItem("href").getNodeValue()));
                        Pattern p = Pattern.compile("[^_]font-family([^;]*|\\s*)*;");
                        Matcher m = p.matcher(content);
                        while (m.find()){
                            String fonts = m.group().split(":")[1];
                            String[] listFonts = fonts.split(",");
                            for(String font: listFonts){
                                String finalFontName = font.strip().replace(";","").replace("\"","");
                                if(!this.fonts.contains(finalFontName))
                                    this.fonts.add(finalFontName);

                            }
                        }
                        /*Font f = Font.createFont(Font.TRUETYPE_FONT, new FileInputStream(Paths.get(this.epubRootFolder.toString(), itemsRef.item(i).getAttributes().getNamedItem("href").getNodeValue().toString()).toString()));
                        Pair<String, String> font = new Pair<>(
                                f.getName(),
                                Paths.get(this.epubRootFolder.toString(), itemsRef.item(i).getAttributes().getNamedItem("href").getNodeValue()).toString()
                        );*/

                        //this.fonts.add(font);
                    }
                }
            } else {
                throw new Exception("Epub vide");
            }
        } else {
            throw new Exception("Rootfile n'existe pas");
        }
    }



    public HashMap<Integer, Pair<String, String>> getEpubPages(){
        return this.pages;
    }


    public int countPages()
    {
        return this.pages.size();
    }
    /**
     * Zip l'epub
     * @param output
     * @param workspace
     * @throws IOException
     */
    public void zipEpub(Path output, Path workspace) throws IOException {
        if(output.toFile().exists()){
            output.toFile().delete();
        }

        Path realOutput = Files.createFile(output);

        if (!realOutput.toString().endsWith(".epub")){
            realOutput = Paths.get(realOutput.toString() + ".epub");
        }
        try (ZipOutputStream zs = new ZipOutputStream(Files.newOutputStream(realOutput))) {
            Files.walk(workspace)
                    .filter(path -> !Files.isDirectory(path))
                    .forEach(path -> {
                        ZipEntry zipEntry = new ZipEntry(workspace.relativize(path).toString());
                        try {
                            zs.putNextEntry(zipEntry);
                            Files.copy(path, zs);
                            zs.closeEntry();
                        } catch (IOException e) {
                            System.err.println(e);
                        }
                    });
        }
    }

    /**
     * Dezip l'epub
     * @param epub
     * @param workspace
     * @throws IOException
     */
    public void extractEpub(Path epub, Path workspace) throws IOException {
        ZipInputStream zipInputStream = new ZipInputStream(new FileInputStream(epub.toFile()));
        ZipEntry zipEntry = zipInputStream.getNextEntry();

        while (zipEntry != null) {

            boolean isDirectory = zipEntry.getName().endsWith(File.separator);
            Path realTarget = workspace.resolve(zipEntry.getName()).normalize();

            if (zipEntry.getName().endsWith(File.separator) || zipEntry.getName().endsWith("/")){
                isDirectory = true;
            }

            if (isDirectory){
                Files.createDirectories(realTarget);
            } else {
                if (realTarget.getParent() != null){
                    if (Files.notExists(realTarget.getParent())){
                        Files.createDirectories(realTarget.getParent());
                    }
                }
                Files.copy(zipInputStream, realTarget, StandardCopyOption.REPLACE_EXISTING);
            }

            zipEntry = zipInputStream.getNextEntry();
        }

        zipInputStream.closeEntry();
    }

    /**
     * Copie la font dans l'epub
     * @param font
     * @throws IOException
     */
    public void addFont(Path font) throws Exception {
        Path realPath = Paths.get(this.epubRootFolder.toString(), "CFonts");

        Font f = Font.createFont(Font.TRUETYPE_FONT, new FileInputStream(font.toString()));
        String name = f.getName();


        Path realFontPath = Paths.get(this.epubRootFolder.toString(), "CFonts", font.toFile().getName());

        if (Files.notExists(realPath)){
            Files.createDirectory(realPath);
        }

        if (Files.exists(realFontPath)){
            Files.delete(realFontPath);
        }

        Files.copy(font, realFontPath);

        this.addFontImportToEpub(name,font.toFile().getName());
        Path specificStylePath = Paths.get(this.epubRootFolder.toString(), "CStyles", "custom_specific.css");
        FileOutputStream fos = new FileOutputStream(specificStylePath.toString(), true);
        fos.write(("\n@font-face {\n" +
                "font-family:" + f.getFamily() + ";\n" +
                "font-weight:" + (f.isBold() ? "bold" : "normal") + ";\n" +
                "font-style:" + (f.isItalic() ? "italic" : "normal") + ";\n" +
                "src:url(\"../CFonts/" +font.toFile().getName() +  "\");\n" +
                "}").getBytes());
        fos.close();
        this.fonts.add(f.getFamily());
    }

    public ArrayList<String> getFonts() {
        return this.fonts;
    }

    public Optional<String> getExtensionByStringHandling(String filename) {
        return Optional.ofNullable(filename)
                .filter(f -> f.contains("."))
                .map(f -> f.substring(filename.lastIndexOf(".") + 1));
    }

    /**
     * Créé et importe les fichiers CSS
     * @throws Exception
     */
    public void createCss() throws Exception {
        Path stylesPath = Paths.get(this.epubRootFolder.toString(), "CStyles");
        Path realPathCustomG = Paths.get(this.epubRootFolder.toString(), "CStyles", "custom_global.css");
        Path realPathCustomS = Paths.get(this.epubRootFolder.toString(), "CStyles", "custom_specific.css");

        if (!Files.exists(stylesPath)){
            Files.createDirectory(stylesPath);
        }

        if (!Files.exists(realPathCustomG)){
            Files.createFile(realPathCustomG);
            this.addCssImportToEpub("global");
        }
        if (!Files.exists(realPathCustomS)){
            Files.createFile(realPathCustomS);
            this.addCssImportToEpub("specific");
        }

    }

    /**
     * Importe le css dans l'ensemble de l'EPUB
     * @param name
     * @throws Exception
     */
    public void addCssImportToEpub(String name) throws Exception{
        Document rootDocument = buildDocument(new File(this.epubRootFile.toString()));
        XPath xPath = XPathFactory.newInstance().newXPath();
        NodeList rootCssImports = (NodeList) xPath.evaluate(String.format("/package/manifest/item[@id='custom_%s_stylesheet']", name), rootDocument, XPathConstants.NODESET);

        //Le css n'est pas encore importé
        if (rootCssImports.getLength() == 0) {
            Node manifestNode = (Node) xPath.evaluate("/package/manifest", rootDocument, XPathConstants.NODE);
            Element itemCssImport = rootDocument.createElement("item");

            itemCssImport.setAttribute("id", String.format("custom_%s_stylesheet", name));
            itemCssImport.setAttribute("href", String.format("CStyles/custom_%s.css", name));
            itemCssImport.setAttribute("media-type", "text/css");
            manifestNode.appendChild(rootDocument.adoptNode(itemCssImport)); //On ajoute la node au document en spécifiant le parent

            this.transformerSave(rootDocument, new File(this.epubRootFile.toString())); //Sauvegarde du document
        }

        int globalTagCounter = 0;

        //On importe le css dans tout les fichiers html
        for(Map.Entry<Integer, Pair<String, String>> page : this.pages.entrySet()){
            File pageFile = new File(page.getValue().getValue());
            Document pageDocument = buildDocument(pageFile);
            NodeList htmlLinkNodes = (NodeList) xPath.evaluate(String.format("/html/head/link[@id='custom_%s_stylesheet']", name), pageDocument, XPathConstants.NODESET);

            //Le CSS n'est pas référencé dans la page
            if (htmlLinkNodes.getLength() == 0) {
                Node htmlHeadNode = (Node)xPath.evaluate("/html/head", pageDocument, XPathConstants.NODE);
                Element htmlCssImport = pageDocument.createElement("link");
                String nonAbsolutePath = page.getValue().getValue().toString().substring(this.epubRootFolder.toString().length() + 1, page.getValue().getValue().length()).replace(File.separator, "/");
                int parents = nonAbsolutePath.split("/").length - 1;

                nonAbsolutePath = String.join("", Collections.nCopies(parents, "../"));

                htmlCssImport.setAttribute("id", String.format("custom_%s_stylesheet", name));
                htmlCssImport.setAttribute("href", String.format("%sCStyles/custom_%s.css", nonAbsolutePath, name));
                htmlCssImport.setAttribute("rel", "stylesheet");
                htmlCssImport.setAttribute("type", "text/css");
                htmlHeadNode.appendChild(pageDocument.adoptNode(htmlCssImport));

                for(String tag : Epub.tags){
                    NodeList nodeTag = (NodeList) xPath.evaluate(String.format("//%s", tag), pageDocument, XPathConstants.NODESET);
                    for(int i = 0; i < nodeTag.getLength(); i++){
                        Node item = nodeTag.item(i);
                        Element element = (Element)item;
                        // Pas d'attribut donc on ajoute
                        if (!element.hasAttribute("id")){
                            element.setAttribute("id", String.format("%s_%d", tag, globalTagCounter));
                        }
                        globalTagCounter++;
                    }
                }

                this.transformerSave(pageDocument, pageFile); //Sauvegarde du document
            }
        }
    }

    public void addFontImportToEpub(String name, String filename) throws Exception {
        Document rootDocument = buildDocument(new File(this.epubRootFile.toString()));
        XPath xPath = XPathFactory.newInstance().newXPath();
        NodeList rootFontImports = (NodeList) xPath.evaluate(String.format("/package/manifest/item[@id='%s']", name), rootDocument, XPathConstants.NODESET);

        //La font n'est pas encore importée
        if (rootFontImports.getLength() == 0){
            Node manifestNode = (Node) xPath.evaluate("/package/manifest", rootDocument, XPathConstants.NODE);
            Element itemFontImport = rootDocument.createElement("item");

            itemFontImport.setAttribute("id", String.format("%s", name));
            itemFontImport.setAttribute("href", String.format("CFonts/%s", filename));
            itemFontImport.setAttribute("media-type", String.format("application/x-font-%s", filename.split("\\.")[1]));
            manifestNode.appendChild(rootDocument.adoptNode(itemFontImport));

            this.transformerSave(rootDocument, new File(this.epubRootFile.toString()));
        }
    }

    public void createRefsPage() throws Exception {
        if (!Files.exists(Paths.get(this.epubRootFolder.toString(), "notespage.xhtml"))){

            XPath xPath = XPathFactory.newInstance().newXPath();
            int globalRefCounter = 0;

            Document refsBasePage = this.createRefsBasePage();
            Element bodyBasePage = (Element) xPath.evaluate("//body", refsBasePage, XPathConstants.NODE);
            File refsBaseFile = Paths.get(this.epubRootFolder.toString(), "notespage.xhtml").toFile();

            for (Map.Entry<Integer, Pair<String, String>> page : this.pages.entrySet()) {
                File pageFile = new File(page.getValue().getValue());
                String nonAbsolutePath = page.getValue().getValue().toString().substring(this.epubRootFolder.toString().length() + 1, page.getValue().getValue().length()).replace(File.separator, "/");
                String docAbsolutePath = pageFile.getParentFile().getAbsolutePath().replace(refsBaseFile.getParentFile().getAbsolutePath(), "").replace(File.separator, "/") + "/" + pageFile.getName();

                if (docAbsolutePath.startsWith("/")){
                    docAbsolutePath = docAbsolutePath.substring(1);
                }

                Document pageDocument = buildDocument(pageFile);
                NodeList aNotes = (NodeList) xPath.evaluate("//*[@type='noteref']", pageDocument, XPathConstants.NODESET);
                int parents = nonAbsolutePath.split("/").length - 1;

                nonAbsolutePath = String.join("", Collections.nCopies(parents, "../"));

                for(int i = 0; i < aNotes.getLength(); i++){
                    globalRefCounter++;

                    Node node = aNotes.item(i);
                    Element element = (Element)node;
                    String noteId = element.getAttribute("id");
                    String notehref = element.getAttribute("href").replace("#", "");

                    element.setAttribute("href", String.format("%snotespage.xhtml#%s", nonAbsolutePath, "fn" + globalRefCounter));
                    element.setAttribute("id", "fnref" + globalRefCounter);

                    Element liElement = (Element) xPath.evaluate(String.format("//*[@id='%s']", notehref), pageDocument, XPathConstants.NODE);
                    Element ahrefElement = (Element) xPath.evaluate(String.format("//*[@href='#%s']", noteId), pageDocument, XPathConstants.NODE);

                    ahrefElement.setAttribute("href", String.format("%s#fnref%s", docAbsolutePath,  globalRefCounter));
                    liElement.setAttribute("id", "fn" + globalRefCounter);

                }

                this.transformerSave(pageDocument, pageFile);

                Element sectionNode = (Element) xPath.evaluate("//*[@type='endnotes']", pageDocument, XPathConstants.NODE);

                if (sectionNode != null){
                    sectionNode.getParentNode().removeChild(sectionNode);
                    refsBasePage.adoptNode(sectionNode);
                    bodyBasePage.appendChild(sectionNode);
                }

                this.transformerSave(pageDocument, pageFile);
            }

            if (globalRefCounter != 0){
                this.transformerSave(refsBasePage, refsBaseFile);
            } else {
                if (!Files.exists(Paths.get(this.epubRootFolder.toString(), "notespage.xhtml"))){
                    Files.createFile(Paths.get(this.epubRootFolder.toString(), "notespage.xhtml"));
                }
            }
        }
    }

    private Document createRefsBasePage() throws Exception {
        Files.deleteIfExists(Paths.get(this.epubRootFolder.toString(), "notespage.xhtml"));
        Files.createFile(Paths.get(this.epubRootFolder.toString(), "notespage.xhtml"));

        File notesFile = new File(this.getPage(0));
        Document document = buildDocument(notesFile);
        XPath xPath = XPathFactory.newInstance().newXPath();
        Element body = (Element) xPath.evaluate("//body", document, XPathConstants.NODE);

        while (body.getChildNodes().getLength() != 0){
            Node childBodyNode = body.getChildNodes().item(0);
            childBodyNode.getParentNode().removeChild(childBodyNode);
        }

        Element elementCSStyle = (Element) xPath.evaluate("//link[@id='custom_specific_stylesheet']", document, XPathConstants.NODE);
        Element elementCGStyle = (Element) xPath.evaluate("//link[@id='custom_global_stylesheet']", document, XPathConstants.NODE);
        Element elementTitle = (Element) xPath.evaluate("//title", document, XPathConstants.NODE);

        elementCSStyle.setAttribute("href", "CStyles/custom_global.css");
        elementCGStyle.setAttribute("href", "CStyles/custom_specific.css");
        elementTitle.setTextContent("Notes de bas de page");

        Element hNotes = document.createElement("h1");

        hNotes.setTextContent("Notes de bas de page");
        body.appendChild(hNotes);

        return document;
    }

    /**
     * Build le DOM du document
     * @param document
     * @return
     * @throws Exception
     */
    public static Document buildDocument(File document) throws Exception {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        factory.setIgnoringComments(true);

        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();

        documentBuilderFactory.setValidating(false);
        documentBuilderFactory.setNamespaceAware(true);
        documentBuilderFactory.setFeature("http://xml.org/sax/features/namespaces", false);
        documentBuilderFactory.setFeature("http://xml.org/sax/features/validation", false);
        documentBuilderFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
        documentBuilderFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);

        DocumentBuilder builder = documentBuilderFactory.newDocumentBuilder();

        return builder.parse(document);
    }

    /**
     * Sauvegarde le document à la destination donnée
     * @param document
     * @param destination
     * @throws TransformerException
     */
    private void transformerSave(Document document, File destination) throws TransformerException {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(document);
        StreamResult file = new StreamResult(destination);

        document.normalizeDocument();
        transformer.transform(source, file);
    }

    public String getPage(int page) throws Exception {
        return this.pages.get(page).getValue();
    }

    public Path getEpubRootFolder() {
        return epubRootFolder;
    }

}
