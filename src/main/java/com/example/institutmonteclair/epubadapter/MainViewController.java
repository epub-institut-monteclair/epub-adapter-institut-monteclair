package com.example.institutmonteclair.epubadapter;

import com.jfoenix.controls.JFXAlert;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.awt.*;
import java.io.File;

public class MainViewController {

    public WebViewController webViewController;
    public OptionViewController optionViewController;

    private Stage stage;

    protected static Epub epub;
    protected Fonts fonts;
    protected SaveManager savemanager;
    private boolean isDialog = false;

    JFXDialog dialogPane;


    @FXML
    public void initialize() {

        // Recuperation de la taille de l'écran
        Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
        int width = (int)size.getWidth();
        int height = (int)size.getHeight()-50;

        this.setSize(width,height);

        this.optionViewController.init(this);
        this.webViewController.init(this);

        this.savemanager = SaveManager.getInstance();
        this.fonts = new Fonts(this.savemanager.getFontsWorkspace());




    }


    public void createDialog()
    {

        JFXDialogLayout content= new JFXDialogLayout();
        content.setHeading(new Text("Sauvegarde"));
        content.setBody(new Text("Voulez-vous sauvegarder les modifications avant de quitter ?"));
        StackPane stackpane = new StackPane();
        dialogPane = new JFXDialog(stackpane , content, JFXDialog.DialogTransition.CENTER);
        isDialog = true;
        JFXButton buttonOui=new JFXButton("Oui");
        JFXButton buttonNon=new JFXButton("Non");
        JFXButton buttonAnnuler=new JFXButton("Annuler");


        buttonOui.setOnAction(new EventHandler<ActionEvent>(){
            @Override
            public void handle(ActionEvent event){

                optionViewController.getBtn_save().fire();
                dialogPane.close();
                Platform.exit();
                System.exit(0);
                isDialog = false;
            }
        });
        buttonNon.setOnAction(new EventHandler<ActionEvent>(){
            @Override
            public void handle(ActionEvent event){

                dialogPane.close();
                Platform.exit();
                System.exit(0);
                isDialog = false;
            }
        });
        buttonAnnuler.setOnAction(new EventHandler<ActionEvent>(){
            @Override
            public void handle(ActionEvent event){
                isDialog = false;
                dialogPane.close();
                dialogPane.setFocusTraversable(true);
                ((GridPane)stage.getScene().getRoot()).getChildren().remove(stackpane);

            }
        });
        content.setActions(buttonOui,buttonNon,buttonAnnuler);

        ((GridPane)stage.getScene().getRoot()).add(stackpane,0,0);

    }
    /**
     * @author Julien Laffely
     * @param width largeur de l'écran
     * @param height hauteur de l'écran
     * Méthode qui va adapter la taille de la fenêtre en fonction de l'écran
     */
    public void setSize(int width , int height){
        this.optionViewController.setPrefSize(2*width/5,height);
        /*Node vbox = this.optionViewController.getChildrenUnmodifiable().get(0);
        ((HBox)vbox).setPrefSize(3*width/5,height);

        this.webViewController.setPrefSize(3*width/5,height);
        Node webview = this.webViewController.getChildrenUnmodifiable().get(0);
        ((WebView)webview).setPrefSize(3*width/5,height-70);

        Node btnbar = this.webViewController.getChildrenUnmodifiable().get(1);
        ((ButtonBar)btnbar).setPrefSize(3*width/5,70);*/

    }


    /**
     * @author Julien Laffely
     * @return WebViewController
     */
    public WebViewController getWebViewController(){
        return this.webViewController;
    }

    /**
     * @author Julien Laffely
     * @return OptionViewController
     */
    public OptionViewController getOptionViewController(){
        return this.optionViewController;
    }

    public void setStage(Stage stage){
        this.stage =stage;
        this.stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                if(epub == null || webViewController.getHistory().size() == 1 || optionViewController.getIsSaved()){
                    Platform.exit();
                    System.exit(0);
                }
                else if (!isDialog)
                {
                    createDialog();
                    stage.show();
                    dialogPane.show();

                }
                event.consume();
            }
        });
    }

    public Stage getStage(){
        return this.stage;
    }

    public boolean ouverture_EPUB(File file)
    {
        savemanager.deleteEpubWorkspace();
        if(file!=null){
            try {
                epub = new Epub(file.getPath(),savemanager.getEpubWorkspace());

                getWebViewController().open_xhtml(epub.getPage(0));
                getWebViewController().firstPageButton();
                getWebViewController().initPageNumbers();
                return true;

            } catch (Exception e) {
                e.printStackTrace();

            }

        }

        return false;
    }

    public static Epub getEpub() {
        return epub;
    }
    public Fonts getFonts() {
        return fonts;
    }



}
