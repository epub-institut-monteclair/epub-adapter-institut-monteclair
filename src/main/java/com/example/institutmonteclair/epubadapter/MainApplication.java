    package com.example.institutmonteclair.epubadapter;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.*;
import javafx.stage.Stage;

import javax.swing.*;
import java.awt.*;
import javafx.scene.image.Image;
import java.io.IOException;
import java.awt.event.KeyListener;

/**
 * @author Julien Laffely
 * Chargement de la scène principale
 * Mise en place des combinaision de touches Ctrl X et ctrl Y
 */
public class MainApplication extends Application {
    private static final int INDETERMINATE = -1;
    private int previousKeyPressed = INDETERMINATE;


    @Override
    public void start(Stage stage) throws IOException {
        // Recuperation de la taille de l'écran
        Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
        int width  = 1200;
        int height = 700;

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("MainView.fxml"));
        Parent root = fxmlLoader.load();
        Scene scene = new Scene(root, width, height);
        scene.getStylesheets().add(getClass().getResource("combo-box.css").toExternalForm());
        try
        {
            Image image = new Image(getClass().getResourceAsStream("Epub-Adapter.png"));
            stage.getIcons().add(image);
            System.out.println("OKOK");
        }
        catch (Exception e)
        {
            System.out.println(e);
        }

        // Attribution des méthodes de changements de styles aux combinaisons de touches CTRL Z et CTRL Y
        /*scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent evt) {
                if( previousKeyPressed == INDETERMINATE )
                    previousKeyPressed = evt.getCode().getCode();
                else if( previousKeyPressed==KeyCode.CONTROL.getCode() && evt.getCode().getCode()==KeyCode.Z.getCode() ){
                    ((MainViewController)fxmlLoader.getController()).getWebViewController().previousStyleSheets();
                }
                else if( previousKeyPressed==KeyCode.CONTROL.getCode() && evt.getCode().getCode()==KeyCode.Y.getCode() ){
                    ((MainViewController)fxmlLoader.getController()).getWebViewController().nextStyleSheets();
                }
            }
        });*/

        scene.addEventFilter(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            final KeyCombination ctrlY = new KeyCodeCombination(KeyCode.Y, KeyCombination.CONTROL_DOWN);
            final KeyCombination ctrlZ = new KeyCodeCombination(KeyCode.Z, KeyCombination.CONTROL_DOWN);
            final KeyCombination ctrlS = new KeyCodeCombination(KeyCode.S, KeyCombination.CONTROL_DOWN);
            final KeyCombination ctrlO = new KeyCodeCombination(KeyCode.O, KeyCombination.CONTROL_DOWN);

            @Override
            public void handle(KeyEvent event) {
                if(ctrlY.match(event)){
                    ((MainViewController)fxmlLoader.getController()).getWebViewController().nextStyleSheets();

                }else if(ctrlZ.match(event)){
                    ((MainViewController)fxmlLoader.getController()).getWebViewController().previousStyleSheets();

                }else if(ctrlS.match(event)){
                    ((MainViewController)fxmlLoader.getController()).getOptionViewController().getBtn_save().fire();
                }else if(ctrlO.match(event)){
                    ((MainViewController)fxmlLoader.getController()).getOptionViewController().getBrowse().fire();

                }
            }
        });

        /*scene.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent evt) {
                if( previousKeyPressed == evt.getCode().getCode() )
                    previousKeyPressed = INDETERMINATE;
            }
        });*/

        stage.setTitle("EPUB-Adapter");
        stage.setScene(scene);
        stage.setResizable(true);
        stage.setMinWidth(width);
        stage.setMinHeight(height);
        ((MainViewController)fxmlLoader.getController()).setStage(stage);

        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }

}
