package com.example.institutmonteclair.epubadapter;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Point3D;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.PickResult;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import org.javatuples.Quintet;
import org.jdom2.*;
import org.jdom2.filter.Filters;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.jdom2.xpath.XPathExpression;
import org.jdom2.xpath.XPathFactory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Entities;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import java.net.URL;
import javax.swing.text.html.StyleSheet;
import java.awt.*;
import java.io.*;

import java.lang.reflect.Array;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.javatuples.Triplet;
import netscape.javascript.JSObject;


public class WebViewController extends GridPane
{

    @FXML
    private WebView webview;
    @FXML
    private Button previous;
    @FXML
    private Button next;

    private boolean is_reloaded;

    private int numpage;

    private File f;

    protected String _selectionTagName;
    protected String _selectionId;
    protected String _selection_element;
    private static final String img_data_prefix = "data_";
    private MainViewController main;
    private int id_actual_styleSheets;
    private int id_previous_stylesheets;

    private ArrayList<String> history;
    private ArrayList<Quintet<String,String,String,String,String>> saved_history;

    private double click_x;
    private double click_y;


    /**
     * @author Julien Laffely
     * Constructeur du WebViewController : il permet d'avoir un element fxml custom
     */
    public WebViewController(){
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("WebView.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    /**
     * @author Julien Laffely && GRENON Guillaume
     * Méthode automatiquement appelé lorsque le WebviewController est instanciée
     * Initialisation à vide des attribut de selection
     * Ouvreture d'un fichier test
     * Deux evenements qui vont permettre de selectionner les elements cliqer et de leur attribuer une animation
     */
    @FXML
    public void initialize() {
        // On initialise les selection a vide
        this._selectionTagName="";
        this._selectionId="";
        this.id_actual_styleSheets=-1;
        this.is_reloaded=false;

        this.previous.setVisible(false);
        this.next.setVisible(false);

        this.history = new ArrayList<String>();
        this.saved_history = new ArrayList<Quintet<String,String,String,String,String>>();


        // Event qui va permettre d'ajouter la class d'animation à chaque fois qu'une page sera charger
        this.webview.getEngine().getLoadWorker().stateProperty().addListener(
                new ChangeListener<Worker.State>() {
                    @Override
                    public void changed(ObservableValue<? extends Worker.State> observable, Worker.State oldValue, Worker.State newValue) {
                        if(newValue==Worker.State.SUCCEEDED ){
                            org.w3c.dom.Element style = webview.getEngine().getDocument().createElement("style");
                            style.setAttribute("type", "text/css");
                            style.setTextContent("@keyframes zoom{0%{transform: scale(1,1);} 50%{transform : scale(1.02,1.02);} 100% {transform : scale(1,1);}}" +
                                    " .blink{animation: zoom 1s ;animation-iteration-count: 2;}" +
                                    "html{margin-right : 50px}");
                            webview.getEngine().getDocument().getElementsByTagName("head").item(0).appendChild(style);
                            /*webview.getEngine().executeScript("var style = document.createElement(\"style\");" +
                                    "style.type=\"text/css\";" +
                                    "style.innerHTML=\"@keyframes zoom{0%{transform: scale(1,1);} 50%{transform : scale(1.02,1.02);} 100% {transform : scale(1,1);}}\";" +
                                    "style.innerHTML+=\" .blink{animation: zoom 1s ;animation-iteration-count: 2;}\";" +
                                    "style.innerHTML+=\" html{margin-right : 50px}\";" +
                                    "document.getElementsByTagName('head')[0].appendChild(style);");
*/
                            // Initialisation du tableau qui contient l'historique des styles de la page courante
                            webview.getEngine().executeScript("let styles = Array();");
                            webview.getEngine().executeScript("styles.splice();");
                            id_actual_styleSheets=-1;
                            
                            addStyleSheets("","","","",false);

                            if(!is_reloaded){
                                is_reloaded=true;
                                webview.getEngine().reload();
                            }

                            replaceHistory();


                        }


                    }
                });

        // Event qui va permettre de recuperer les élements selectionner et d'attribuer une animation à la selection courante
        this.webview.addEventFilter(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {

                if(webview.getEngine().getDocument()!=null) {

                    // On récupère l'endroit ou le clique a été fait, on récupère les coordonnées du clique
                    PickResult resultat= mouseEvent.getPickResult();
                    Point3D coordonneesClick = resultat.getIntersectedPoint();

                    double x = coordonneesClick.getX();
                    double y = coordonneesClick.getY();

                    // Si il y avait une selection alors on retire l'animation de l'ancienne selection

                    if(!_selectionTagName.isEmpty())
                        webview.getEngine().executeScript("var elements = document.getElementsByTagName(\""+_selectionTagName+"\");" +
                                "for(let step = 0 ; step < elements.length ; step++)" +
                                "if(elements[step].classList.contains(\"blink\"))" +
                                "elements[step].classList.remove(\"blink\");");


                    // On récupère l'element selection graceaux coordoonées du clique et ensuite on récupère son TagName et son
                    String ancienTag = _selectionTagName;
                    // Exclusion des balises non modifiable
                    _selectionTagName = (String) webview.getEngine().executeScript("var okTag = " +
                            "[\"p\",\"h1\",\"h2\",\"h3\",\"h4\",\"h5\",\"h6\"];" +
                            "var element = document.elementFromPoint("+x+","+y+");" +
                            "if(element.tagName==\"img\")" +
                            "{ element.tagName;}" +
                            "else{" +
                            "while(element.parentNode!=null && okTag.includes(element.parentNode.tagName)){" +
                            "   element = element.parentNode ;}" +
                            "while(element.children.length==1 && okTag.includes(element.children[0].tagName)){" +
                            "   element = element.children[0];" +
                            "}" +
                            "if(element.children.length==2 && element.children[0].tagName == 'img' && element.children[1].tagName == 'data'){" +
                            "   element = element.children[1];" +
                            "}" +
                            "element.tagName;" +
                            "}");

                // Retourne le contenu html de l'élément cliqué, sert pour l'html editor
                _selection_element = (String) webview.getEngine().executeScript("var excludedTag = " +
                        "[\"html\",\"body\",\"span\",\"div\",\"head\",\"link\",\"meta\",\"script\"," +
                        "\"style\",\"abbr\",\"blockquote\",\"cite\",\"q\",\"sup\",\"sub\",\"em\"," +
                        "\"figure\",\"\",\"audio\",\"source\",\"br\",\"progress\",\"li\",\"dl\"," +
                        "\"dt\",\"dd\",\"tr\",\"th\",\"td\",\"thead\",\"tbody\",\"tfoot\",\"input\"," +
                        "\"textarea\",\"select\",\"option\",\"optgroup\",\"header\",\"nav\",\"footer\"," +
                        "\"section\",\"article\",\"aside\"];" +
                        "var element = document.elementFromPoint("+x+","+y+");" +
                        "if(element.tagName==\"img\")" +
                        "{ element.tagName;}" +
                        "else{" +
                        "while(element.parentNode!=null && !excludedTag.includes(element.parentNode.tagName)){" +
                        "element = element.parentNode ;}" +
                        "element.innerHTML;" +
                        "}");

                    _selectionId = (String) webview.getEngine().executeScript(
                            "var element = document.elementFromPoint("+x+","+y+");" +
                                    "element.id;");

                    if(!_selectionTagName.isEmpty() && !_selectionTagName.equals("undefined")) {
                        if (_selectionTagName.equals("html") || _selectionTagName.equals("body") || _selectionTagName.equals("span") || _selectionTagName.equals("div") || _selectionTagName.equals("section"))
                            _selectionTagName = ancienTag;
                        else {
                            // On donne l'animation à\aux l'élément\éléments séléctionné(s)
                            webview.getEngine().executeScript("var elements = document.getElementsByTagName(\"" + _selectionTagName + "\");" +
                                    "for(let step = 0 ; step < elements.length ; step++)" +
                                    "elements[step].classList.add(\"blink\");");

                        }
                    }

                    if(!_selectionTagName.isEmpty() && !_selectionTagName.equals("undefined")&& !_selectionTagName.equals("img")) {
                        // Chargement lors d'un clic sur un élément dans l'optionView -------------------------------------------------------------------------------
                        click_x=x;
                        click_y=y;
                        defaultAll(click_x,click_y);
                    }
                    else if(_selectionTagName.equals("img")){
                        main.getOptionViewController().imgSelected(true);
                        main.getOptionViewController().imgReplaced(false);

                    }
                }
            }
        });


        // Evenement qui permet de passer à la page suivante lorsqu'on clique sur le bouton "page suivante"
        // Cette evenement gère également l'affichage des boutons en fonction du numero de page
        this.previous.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                --numpage;
                try {
                    is_reloaded=false;
                    id_previous_stylesheets=id_actual_styleSheets;
                    open_xhtml(main.getEpub().getPage(numpage));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if(numpage==0)
                    firstPageButton();
                else middlePageButton();
                main.getOptionViewController().resetSlider();
                main.getOptionViewController().imgSelected(false);
                main.getOptionViewController().defaultLabel("Aucun élement sélectionné");
                //load_CSS_InMemory(save_CSS_InMemory());
            }
        });

        // Evenement qui permet de passer à la page précédente lorsqu'on clique sur le bouton "page précédente"
        // Cette evenement gère également l'affichage des boutons en fonction du numero de page
        this.next.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                ++numpage;
                try {
                    is_reloaded=false;
                    id_previous_stylesheets=id_actual_styleSheets;
                    open_xhtml(main.getEpub().getPage(numpage ));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    if(numpage==main.epub.countPages()-1)
                        lastPageButton();
                    else middlePageButton();

                } catch (Exception e) {
                    e.printStackTrace();
                }
                // FAIRE UN RESET ALL
                main.getOptionViewController().resetSlider();
                main.getOptionViewController().imgSelected(false);
                main.getOptionViewController().defaultLabel("Aucun élement sélectionné");

                //load_CSS_InMemory(save_CSS_InMemory());
            }
        });

        // Évènement double clic : ouvre la page d'édition de l'HTML (dans las classe OptionViewController)
        this.webview.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if(mouseEvent.getButton().equals(MouseButton.PRIMARY))
                {
                    if(mouseEvent.getClickCount() == 2)
                    {
                        main.getOptionViewController().open_HTMLEditor();
                    }
                }
            }
        });
    }

    /**
     * @author GRENON Guillaume && GAUTRON Rémy
     * @param new_html_value texte entré dans le HTMLEditor à remplacer dans le texte HTML du document
     *
     * Methode remplaçant, dans le document HTML, le texte de l'élément cliqué par le texte écrit dans la fenêtre HTMLEditor
     */
    public void set_inner_HTML(String new_html_value)
    {
        // Crée un pattern reconnaissant une regex qui permet de prendre tout le contenu HTML à l'intérieur du body
        Pattern p = Pattern.compile("<body[^>]*>(.*?)</body>", Pattern.DOTALL);

        // Crée un matcher qui applique ce pattern au texte passé en paramètre
        Matcher interieurbody = p.matcher(new_html_value);

        boolean b = interieurbody.find();

        if(b)
        {
            try{
                String base = "<html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:epub=\"http://www.idpf.org/2007/ops\" xml:lang=\"fr-fr\">"+ interieurbody.group(1).trim() +"<html/>";

                org.jsoup.nodes.Document d = Jsoup.parse(base);
                d.outputSettings().syntax(org.jsoup.nodes.Document.OutputSettings.Syntax.xml);
                d.outputSettings().escapeMode(Entities.EscapeMode.xhtml);
                String text = d.html();

                InputStream is = new ByteArrayInputStream(text.getBytes(StandardCharsets.UTF_8));

                File html = new File(main.epub.getPage(this.numpage));
                Document doc = new SAXBuilder().build(html);
                XPathExpression<org.jdom2.Element> xpe = XPathFactory.instance().compile(
                        "//xhtml:"+this._selectionTagName + "[@id='" + this._selectionId + "']", Filters.element(),null, Namespace.getNamespace("xhtml", "http://www.w3.org/1999/xhtml")
                );
                Element e = xpe.evaluateFirst(doc);

                SAXBuilder sb = new SAXBuilder();
                Document newdoc = sb.build(is);

                XPathExpression<org.jdom2.Element> xpe2 = XPathFactory.instance().compile(
                        "//xhtml:body", Filters.element(),null, Namespace.getNamespace("xhtml", "http://www.w3.org/1999/xhtml")
                );
                Element body = xpe2.evaluateFirst(newdoc);

                e.removeContent();
                for(Content c : body.getContent()){
                    if(c instanceof Element) {
                        Element el = ((Element) c).clone();
                        el.detach();
                        e.addContent(el);
                    }else if(c instanceof Text){
                        Text el = ((Text) c).clone();
                        el.detach();
                        e.addContent(el);
                    }

                }

                Writer fw = new OutputStreamWriter(new FileOutputStream(html), StandardCharsets.UTF_8);
                XMLOutputter xout = new XMLOutputter(Format.getPrettyFormat().setEncoding("UTF-8"));

                xout.output(doc, fw);

                this.reload();
            }catch(Exception e){
                System.out.println(e);
            }
            // Script prenant l'id de l'élément cliqué et remplace le texte de l'élément a cet par le nouveau texte (en vérifiant que tous les guillemets sont échappés et trim la chaine)
           // webview.getEngine().executeScript("document.getElementById(\"" + this._selectionId + "\").innerHTML = \"" + interieurbody.group(1).replaceAll("\"", "\\\\\"").trim() + "\"");
        }
    }




        public void reload() {
            try {
                is_reloaded=false;
                id_previous_stylesheets=id_actual_styleSheets;
                open_xhtml(main.getEpub().getPage(numpage ));
            } catch (Exception e) {
                e.printStackTrace();
            }

        }


    /**
     * @author Julien Laffely && Guillaume GRENON
     * @param filename lien du fichier html à afficher
     * Méthode qui ouvre une page html
     */
    public void open_xhtml(String filename) {
        this.f = new File(filename);
        this.webview.getEngine().load(this.f.toURI().toString());
        this.webview.getEngine().setJavaScriptEnabled(true);
    }

    /**
     * @author Julien Laffely
     * @param x coordonnée x du dernier click sur la webview
     * @param y coordonnée y du dernier click sur la webview
     * Méthode qui permet de mettre par default tous les éléments de l'option view lors d'un click sur la webview
     */
    public void defaultAll(double x, double y){
        // Label élément sélectionné
        String element = _selectionTagName;

        main.getOptionViewController().defaultLabel(element);


        // Style de caractère

        // Gras
        String bold = (String) webview.getEngine().executeScript("var el = document.getElementsByTagName(\"" + _selectionTagName + "\")[0];\n" +
                "var style = window.getComputedStyle(el, null).getPropertyValue('font-weight');\n" +
                "String(style); ");


        main.getOptionViewController().defaultBold(bold);

        // Italique
        String italic = (String) webview.getEngine().executeScript("var el = document.getElementsByTagName(\"" + _selectionTagName + "\")[0];\n" +
                "var style = window.getComputedStyle(el, null).getPropertyValue('font-style');\n" +
                "String(style); ");


        main.getOptionViewController().defaultItalic(italic);

        // Souligne
        String underline = (String) webview.getEngine().executeScript("var el = document.getElementsByTagName(\"" + _selectionTagName + "\")[0];\n" +
                "var style = window.getComputedStyle(el, null).getPropertyValue('text-decoration');\n" +
                "String(style); ");


        main.getOptionViewController().defaultSouligne(underline);


        // Police de caractère
        String police = (String) webview.getEngine().executeScript("var el = document.getElementsByTagName(\"" + _selectionTagName + "\")[0];\n" +
                "var style = window.getComputedStyle(el, null).getPropertyValue('font-family');\n" +
                "String(style); ");


        main.getOptionViewController().defaultPolice(police);

        // Couleur de caractère
        String charColor = (String) webview.getEngine().executeScript("var el = document.getElementsByTagName(\"" + _selectionTagName + "\")[0];\n" +
                "var style = window.getComputedStyle(el, null).getPropertyValue('color');\n" +
                "String(style); ");


        main.getOptionViewController().defaultColor(charColor);


        // Couleur de fond
        String backgroundColor = (String) webview.getEngine().executeScript("var el = document.getElementsByTagName(\"" + _selectionTagName + "\")[0];\n" +
                "var style = window.getComputedStyle(el, null).getPropertyValue('background-color');\n" +
                "String(style); ");


        main.getOptionViewController().defaultBackgroundColor(backgroundColor);


        // Alignement de texte
        String alignement = (String) webview.getEngine().executeScript("var el = document.getElementsByTagName(\"" + _selectionTagName + "\")[0];\n" +
                "var style = window.getComputedStyle(el, null).getPropertyValue('text-align');\n" +
                "String(style); ");


        main.getOptionViewController().defaultAlignement(alignement);
        // Opacité de l'élément
        String opacity = (String) webview.getEngine().executeScript("var el = document.getElementsByTagName(\"" + _selectionTagName + "\")[0];\n" +
                "window.getComputedStyle(el, null).getPropertyValue('opacity');\n");
        if(Double.parseDouble(opacity) == 0.0)
            main.getOptionViewController().defaultOpacity(true);
        else
            main.getOptionViewController().defaultOpacity(false);
        // Slider espacement
        String getSpace_px = (String) webview.getEngine().executeScript("var el = document.getElementsByTagName(\"" + _selectionTagName + "\")[0];\n" +
                "window.getComputedStyle(el, null).getPropertyValue('padding');\n" );

        double getSpace_pt = 0.75* Double.parseDouble(getSpace_px.substring(0,getSpace_px.length()-2));

        main.getOptionViewController().defaultSliderSpace(getSpace_pt);

        // Slider taille text
        String fontSize_px = (String) webview.getEngine().executeScript("var el = document.getElementsByTagName(\"" + _selectionTagName + "\")[0];\n" +
                "window.getComputedStyle(el, null).getPropertyValue('font-size');\n");

        double fontSize_pt = 0.75 * Double.parseDouble(fontSize_px.substring(0, fontSize_px.length() - 2));


        main.getOptionViewController().defaultSlider(fontSize_pt);

        if((Boolean)webview.getEngine().executeScript("document.elementFromPoint("+x+","+y+").classList[0]==\"texte_d_une_image\";")) {
            webview.getEngine().executeScript("document.elementFromPoint("+x+","+y+").id=\"temporaire\"");
            main.getOptionViewController().imgSelected(true);
            main.getOptionViewController().imgReplaced(true);
        }
        else {
            main.getOptionViewController().imgSelected(false);

            // On enleve le potentiel id temporaire de l'ancienne image selectionner
            webview.getEngine().executeScript("if(document.getElementById(\"temporaire\")!=null)" +
                    "document.getElementById(\"temporaire\").id=\"\";");
        }
    }



    /**
     * @author Guillaume GRENON
     * @return String : contenu de tous les fichiers de style
     *
     * Méthode retournant, sous forme de chaine de caractère, le contenu de tous les fichiers de styles appliqués à la WebView courrente
     */
    public String load_CSS_StyleSheet()
    {
        // Script parcourant toutes les règles de toutes les feuilles de styles (appliqués a la webview courante) et les mets sous forme de texte dans une variable nommée tableaux dont tab prendra la valeur finale
        // On déclare une variable tab de type tableau
        return (String) webview.getEngine().executeScript("var tableaux = Array();" +
                // Parcours toutes les feuilles de styles appliqués a la webview
                "for (let i = 0; i < document.styleSheets.length; ++i)" +
                "{" +
                // Parcours toutes les règles de la feuille de style i
                "for (let j = 0; j < document.styleSheets[i].cssRules.length; ++j)" +
                "{" +
                // On ajoute (à la fin du tableau) la règle j de la feuille de style i dans tab
                "tableaux.push(document.styleSheets[i].cssRules[j].cssText);" +
                "}" +
                "}" +
                // On transforme tab, de tableau, à chaine de caractère, oû chaque valeur du tableau est séparée par un '!'
                //Il faut un caractère pas utilisé en CSS car c'est ce caractère qui déterminera le retour à la ligne dans la fonction save() (écriture dans le fichier)
                "tableaux.join('!');");
    }

    /**
     * @author Guillaume GRENON
     * @return String : contenu de toutes les modifications de Style appliqué aux balises HTML
     *
     * Méthode retournant, sous forme de chaine de caractère, le contenu de toutes les modifications de styles faites par l'utilisateur (ou faites de base dans le HTML)
     */
    public String load_CSS_HTML()
    {
        /*
         DANS QUELQUES CAS EXTREME, CETTE FONCTION PEUT NE PAS FAIRE CE QUE L'ON VEUT :
            -SI IL Y A LE TAG !important SUR UNE REGLE, C'EST CE STYLE LA QUI SERA APPLIQUE ET PAS LE DERNIER
            -SI LE FICHIER HTML EST MAL CONSTRUIT CERTAINES BALISES PEUVENT ÊTRE OUBLIES : TESTS NÉCESSAIRES
         */
        return (String) webview.getEngine().executeScript("var customCSS;" +
                "function getElementCSSTextR(element) " +
                "{" +

                // Calcul -----

                // Récupère le nom de la balise dans identifier
                "let identifier = element.tagName.toLowerCase();" +

                /* Finalement pas utile, car on ne veut pas garder ni les classes ni les id

                // Join l'id (s'il en a un) de la balise a identifier
                    //"if( element.id ) { identifier += `#${element.id}`; }" +
                // Si on trouve que l'élément appartient à une classe et qu'elle est sous forme de texte,
                // on l'ajoute la class name
                    //"if( element.className && typeof element.className == 'string' ) { identifier += Array.from(element.classList).map( classname => `.${classname}` ).join(''); }" +

                */

                // On ajoute récupère la règle sous forme de texte css
                "let self = `${identifier}{${element.style.cssText}}`;" +

                // Récursif -----

                // Crée une liste et lui ajoute l'ensemble des sous-éléments du paramètre element de manière récursive
                "let liste = Array" +
                ".from( element.children )" +
                ".map( childElement => getElementCSSTextR( childElement ) );" +
                // Met en ordre la liste
                "liste.unshift( self );" +
                // Joins la liste pour en faire une chaine de texte ayant (comme pour load_CSSFile()) un '!' entre chaque règles
                "return liste.join('!');" +
                "}" +

                // Appel la fonction sur le body du fichier HTML pour englober toutes les balises (on n'appelle pas sur html car la tête n'est pas nécessaire)
                "customCSS = getElementCSSTextR( document.body );");
    }

    /**
     * @author GRENON Guillaume
     * @param tab tableau auquel on vaut enlever les doublons
     * @return la Liste de chaine de caractère sans doublons
     *
     * Méthode prenant en paramètre un tableau et retournant la liste de ce tableau en enlevant ses doublons
     */
    public List<String> remove_Doublons(String[] tab)
    {
        List<String> newListe = new ArrayList<>();
        // On parcourt le tableau
        for (String s : tab) {
            // Si la liste ne contient pas l'élément du tableau à l'indice i, on ajoute cet élément dans la liste
            if (!newListe.contains(s))
                newListe.add(s);
        }

        return newListe;
    }

    public void addFontDynamically(String name, String path){

        Font f = null;
        try {
            f = Font.createFont(Font.TRUETYPE_FONT, new FileInputStream(path));
        } catch (FontFormatException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        /*String js = "var sheet = window.document.styleSheets[0];" +
                "sheet.insertRule(\"@font-face {font-family: "+name+"';font-style:"+(f.isBold() ? "bold" : "normal")+";font-weight:"+(f.isItalic() ? "italic" : "normal")+";src:url('../../../FONTS/"+ Paths.get(path).toFile().getName() + "');}\", sheet.cssRules.length);" ;
        */
        String js = "const styleId = 'font-style-sheet';" +
                "var fontStyleSheet = document.getElementById(styleId);" +
                "if (fontStyleSheet) {" +
                "    fontStyleSheet.parent.removeChild(fontStyleSheet);" +
                "}"+
                "  var newFontStyleSheet = document.createElement(\"style\");" +
                "newFontStyleSheet.id = styleId;" +
                "newFontStyleSheet.textContent = `" +
                "@font-face {" +
                "font-family: "+name+"';" +
                "font-style:"+(f.isBold() ? "bold" : "normal")+";" +
                "font-weight:"+(f.isItalic() ? "italic" : "normal")+";" +
                "src:url('../../../FONTS/"+ Paths.get(path).toFile().getName() + "') format('ttf');" +
                "}`;" +
                "document.head.appendChild(newFontStyleSheet);";
        this.webview.getEngine().executeScript(js);



    }

    public void importCss(Path path) throws IOException {
        Path targetPath = Paths.get(MainViewController.getEpub().getEpubRootFolder().toString(), "CStyles", "custom_global.css");
        if(targetPath.toFile().exists()){
            Files.delete(targetPath);
        }
        Files.copy(path,targetPath);
        this.reload();
    }
    /**
     * @author Guillaume GRENON
     *
     * Méthode permettant d'écrire dans un fichier la chaine de caractère contenant l'ensemble du css aplliqué à la webview gràce au fichier de style et à l'interface
     */
    public void save() {
        // on vide l'historique sauvegarder
        this.saved_history.clear();
        // Déclare un writer vide
        PrintWriter writer = null;

        // Si un epub a bien été ouvert
        Paths.get(MainViewController.getEpub().getEpubRootFolder().toString(), "CStyles", "custom_global.css");
        //On essaye d'ouvrir le fichier
        try
        {
            // Si le fichier est bien ouvert, on le déclare comme fichier dans lequel on va écrire
            writer = new PrintWriter(Paths.get(MainViewController.getEpub().getEpubRootFolder().toString(), "CStyles", "custom_global.css").toString(), StandardCharsets.UTF_8);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        // On déclare une chaine de caractère, avec comme contenu, toutes les règles appliquées à la webview courante + celle appliqué aux balises html
        // Attention à bien mettre le style appliqué aux balises en dernier, car c'est celui que l'on fait avec l'interface
        String style_text = load_CSS_StyleSheet() + load_CSS_HTML();

        // On convertit cette chaine de caractère en tableau
        // A chaque fois que le caractère déterminant "!" (choisi à la fin du script de load()) est détecté, on sépare le texte dans la valeur suivante du tableau
        String[] tab_text = style_text.split("!");

        // On retire tous les doublons du tableau
        List<String> Liste_sans_doublon = remove_Doublons(tab_text);

        // Pour chaque valeur du tableau, on écrit cette valeur dans le fichier, puis on fait un retour à la ligne
        for (String regle : Liste_sans_doublon)
        {
            // On s'assure que le writer n'est pas null (null != vide)
            if (writer != null)
            {
                // Si la ligne n'est pas vide
                if(!Objects.equals(writer.toString(), "")  && !regle.startsWith("@font-face"))
                    writer.println(regle);
            }
        }

        // On s'assure que le writer n'est pas null (null != vide)
        if (writer != null)
        {
            // Ferme le fichier
            writer.close();
        }
    }



    /**
     * @author GRENON Guillaume
     * @param file fichier css choisi par l'utilisateur
     * @throws IOException si il y a un problème avec l'ouverture du fichier
     *
     * Méthode prenant en paramètre un fichier et applique son contenu dans la l'html du document
     */
    public void load_CSS_File(File file) throws IOException
    {
        //On crée un lecteur de fichiers et une chaine de caractère dans laquelle on va mettre le contenu de ce fichier
        BufferedReader in_file = new BufferedReader(new FileReader(file));
        StringBuilder texte_css = new StringBuilder();
        //src sera chaque ligne une par une du fichier
        String src;

        // Tant qu'on n'est pas arrivé à la fin du fichier, on prend la ligne suivante dans le fichier
        while((src = in_file.readLine()) != null)
        {
            // Et on rajoute cette ligne dans la chaine dédiée
            texte_css.append(src);
        }

        /*
            // Enlève les polices de caractères n'étant pas lisibles (temporaire)
            //String escaped_texte_css = texte_css.toString().replaceAll("font-family:.*\".*\"", "");
         */

        // Echappe les guillemets se trouvant dans la chaine de caractères pour ne pas faire de conflits avec la fonction executeScript() (permet de prendre en compte les fonts)
        String escaped_texte_css = texte_css.toString().replaceAll("\"", "\\\\\"");

        // Script écrivant dans une balise de style, le contenu final de cette chaine de caractère
        webview.getEngine().executeScript("var cssInFile = document.createElement(\"style\");" +
                "cssInFile.innerHTML = \"" + escaped_texte_css + "\";" +
                "document.head.appendChild(cssInFile)");
    }

    /**
     * @author Guillaume GRENON
     * @param attribute L'attribut de règle à ajouter ou à modifier
     * @param value La (nouvelle) valeur donnée à l'attribut
     *
     * Méthode permettant de changer une règle (_selection) en lui ajoutant un attribut (attribute, value), ou en le modifiant s'il existe déjà. Si le fond est cliqué, il n'y a pas de modifications effectuée
     *              Attention quand on appelle la fonction : pour les attributs CSS ayant un "-" dans leur nom, il faut les appeler sans (exemple: font-size doit s'écrire fontSize)
     */
    protected void change_CSSRule(String attribute, String value)
    {
        // nb_node, permet d'avoir le nombre de fois que la règle (la balise) cliquée est appelée dans le html (le nombre de <p> dans le html par exemple)
        int nb_node = (int) this.webview.getEngine().executeScript("document.getElementsByTagName(\"" + this._selectionTagName + "\").length");

        // On verifie que l'utilisateur n'ai pas cliqué sur le fond (balise body, balise html ou div)
        if (!Objects.equals(this._selectionTagName, "body") && (!Objects.equals(this._selectionTagName, "html")) && (!Objects.equals(this._selectionTagName, "div")))
        {
            // Pour toutes les balise cliquées (par exemple pour tous les paragraphes), on applique le changement de règle
            for (int i = 0; i < nb_node; ++i)
            {
                this.webview.getEngine().executeScript("document.getElementsByTagName(\"" + this._selectionTagName + "\")[" + i + "].style." + attribute + " = \"" + value + "\" ;");
            }
        }
        main.optionViewController.setIs_saved(false);
    }

    /**
     * @author GRENON Guillaume
     * @return
     */
    public String save_CSS_InMemory()
    {
        String rules = load_CSS_StyleSheet() + load_CSS_HTML();
        String[] tab_text = rules.split("!");
        List<String> Liste_sans_doublon = remove_Doublons(tab_text);

        StringBuilder styleSheets = new StringBuilder();

        for (String  i : Liste_sans_doublon)
        {
            styleSheets.append(i);
        }

        return styleSheets.toString().replaceAll("\n", " ");
    }

    /**
     * @author GRENON Guillaume
     * @param styleSheet
     */
    public void load_CSS_InMemory(String styleSheet)
    {
        String escaped_texte_css = styleSheet.replaceAll("\"", "\\\\\"");

        webview.getEngine().executeScript("var cssInFile = document.createElement(\"style\");" +
                "cssInFile.innerHTML = \"" + escaped_texte_css + "\";" +
                "document.head.appendChild(cssInFile)");
    }

    /**
     * @author Julien Laffely
     * @return
     * Méthode qui retourne le TagName de l'élément sélectionné par l'utilisateur
     */
    public String getSelectedElementTagName(){
        return _selectionTagName;
    }

    /**
     * @author Julien Laffely
     * Méthode qui retourne l'Id de l'élément selectionné par l'utilisateur
     */
    public String getSelectedElementId(){
        return _selectionId;
    }

    /**
     * @author GRENON Guillaume
     * @return le contenu HTML de l'élément sélectionné par l'utilisateur
     */
    public String get_selection_element()
    {
        return _selection_element;
    }

    /**
     * @author GRENON Guillaume
     * @return la webview gérant l'epub
     */
    public WebView getWebview()
    {
        return webview;
    }

    /**
     * @author Julien Laffely
     * Ajout de l'attribut MainController
     */
    public void init(MainViewController main){
        this.main=main;
    }

    /**
     * @author Julien Laffely
     * Affiche uniquement le bouton "page suivante"
     */
    public void firstPageButton(){
        this.previous.setVisible(false);
        this.next.setVisible(true);
    }

    /**
     * @author Julien Laffely
     * Affiche uniquement les boutons "page précédente" et "page"suivante"
     */
    public void middlePageButton(){
        this.previous.setVisible(true);
        this.next.setVisible(true);
    }

    /**
     * @author Julien Laffely
     * Affiche uniquement le bouton "page précédente"
     */
    public void lastPageButton(){
        this.previous.setVisible(true);
        this.next.setVisible(false);
    }

    /**
     * @author Julien Laffely
     * Initialise le nombre de page à 0
     */
    public void initPageNumbers(){
        this.numpage = 0;
    }

    public ArrayList<String> getHistory() {
        return history;
    }

    /**
     * @author Julien Laffely
     * @param styleName Nom du style qui est modifier
     * @param styleValue Nouvelle valeur du style qui a été modifier
     * Ajoute la page de style actuel au tableau qui contient l'historique des modifications
     */


    public void addStyleSheets(String styleName,String styleValue, String realStyleName, String realStyleValue, boolean saveInSavedHistory){
        if(styleName=="" && styleValue=="" && realStyleName=="" && realStyleValue=="") {
            history.clear();
            history.add("Style d'ouverture");
        }
        else{
            for (int i = history.size() - 1; i > this.id_actual_styleSheets; --i) {
                history.remove(history.size() - 1);
                saved_history.remove(saved_history.size()-1);
            }
            history.add(styleName + " " + _selectionTagName + " : " + styleValue);
            if(saveInSavedHistory)
                saved_history.add(new Quintet<String,String,String,String,String>(realStyleName,styleName,_selectionTagName,styleValue,realStyleValue));
        }

        this.main.getOptionViewController().updateHistory(history);

        this.webview.getEngine().executeScript("var tableau = Array();" +
                // Parcours toutes les feuilles de styles appliqués a la webview
                "var elements = document.getElementsByTagName(\"*\");" +
                "for (let i = 0; i < elements.length; ++i)" +

                "       tableau.push(elements[i].style.cssText) ;" +
                "for(let i = styles.length-1 ; i>"+this.id_actual_styleSheets+";--i)" +
                "styles.pop();" +
                "styles.push(tableau);");

        this.id_actual_styleSheets++;
    }

    /**
     * @author Julien Laffely
     * Lors d'un ctrl z applique le style actif précédent
     */
    public void previousStyleSheets(){
        if(this.id_actual_styleSheets>0) {
            int previous_id = this.id_actual_styleSheets-1;
            this.webview.getEngine().executeScript(
                    "var old_style = styles["+previous_id+"];"+
                            "for(let i = 0 ; i < document.getElementsByTagName(\"*\").length; ++i)" +
                            "document.getElementsByTagName(\"*\")[i].style.cssText=old_style[i];");

            for(int i = 0; i <main.getOptionViewController().getBtn_history().getItems().size()-2 ; ++i){
                CheckBox item =  (CheckBox)((CustomMenuItem)main.getOptionViewController().getBtn_history().getItems().get(i)).getContent();
                if(i<this.id_actual_styleSheets)
                    item.setSelected(true);
                else item.setSelected(false);
            }


            this.id_actual_styleSheets--;
        }
    }

    /**
     * @author Julien Laffely
     * Lors d'un clique sur l'historique applique le style selectionné
     */
    public void selectionStyleSheets(int selection){
        this.webview.getEngine().executeScript(
                "var select_style = styles["+selection+"];"+
                        "for(let i = 0 ; i < document.getElementsByTagName(\"*\").length; ++i)" +
                        "document.getElementsByTagName(\"*\")[i].style.cssText=select_style[i];");

        this.id_actual_styleSheets=selection;
    }

    /**
     * @author Julien Laffely
     * Lors d'un ctrl y applique le style actif avant le dernier ctrl z
     */
    public void nextStyleSheets(){
        if(this.id_actual_styleSheets!=(int)this.webview.getEngine().executeScript("styles.length-1;")) {
            int next_id = this.id_actual_styleSheets+1;
            this.webview.getEngine().executeScript(
                    "var next_style = styles["+next_id+"];"+
                            "for(let i = 0 ; i < document.getElementsByTagName(\"*\").length; ++i)" +
                            "document.getElementsByTagName(\"*\")[i].style.cssText=next_style[i];");

            for(int i = 0; i <main.getOptionViewController().getBtn_history().getItems().size()-2 ; ++i){
                CheckBox item =  (CheckBox)((CustomMenuItem)main.getOptionViewController().getBtn_history().getItems().get(i)).getContent();
                if(i<=this.id_actual_styleSheets+1)
                    item.setSelected(true);
                else item.setSelected(false);
            }

            this.id_actual_styleSheets++;
        }
    }

    /**
     * @author Julien Laffely
     * retourne la valeur alternative de l'image en cours de selection
     */

    public String getAltImage(){

        if(this._selectionId.startsWith(img_data_prefix)){
            return this.webview.getEngine().getDocument().getElementById(this._selectionId.substring(img_data_prefix.length())).getAttribute("alt");

        }
        else {
            return this.webview.getEngine().getDocument().getElementById(this._selectionId).getAttribute("alt");
        }
    }

    /**
     * @author Julien Laffely
     * @param newAlt texte de remplacement de l'image
     * @param replaceImg bolleen qui indique si l'image est remplacer ou non
     * Remplace l'image selectionner par le texte saisie (creation d'une nouvelle balise)
     */
    public void replaceImageBy(String newAlt, boolean replaceImg){

        //String base = "<html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:epub=\"http://www.idpf.org/2007/ops\" xml:lang=\"fr-fr\">"+ interieurbody.group(1).trim() +"<html/>";

        //org.jsoup.nodes.Document d = Jsoup.parse(base);
        //d.outputSettings().syntax(org.jsoup.nodes.Document.OutputSettings.Syntax.xml);
        //d.outputSettings().escapeMode(Entities.EscapeMode.xhtml);
        // text = d.html();

       // InputStream is = new ByteArrayInputStream(text.getBytes(StandardCharsets.UTF_8));
        try {
            File html = new File(main.epub.getPage(this.numpage));
            Document doc = new SAXBuilder().build(html);
            XPathExpression<org.jdom2.Element> xpe = XPathFactory.instance().compile(
                    "//xhtml:*[@id='" + this._selectionId + "']", Filters.element(), null, Namespace.getNamespace("xhtml", "http://www.w3.org/1999/xhtml")
            );
            Element e = xpe.evaluateFirst(doc);
            if (!this._selectionId.startsWith(img_data_prefix)) {

                e.setAttribute("alt", newAlt);
                if (replaceImg) {
                    if(e.getChildren("data", e.getNamespace()).isEmpty()){
                        Element data = new Element("data");

                        data.setNamespace(Namespace.getNamespace("http://www.w3.org/1999/xhtml"));
                        data.setAttribute("id", img_data_prefix+this._selectionId);
                        data.setAttribute("value", this._selectionId);
                        data.setAttribute("class", "texte_d_une_image");
                        Text content = new Text(newAlt);
                        data.addContent(content);
                        String defaultStyle = "";
                        boolean isStyle = true;
                        try{
                            e.getAttribute("style").isSpecified();
                        }catch (NullPointerException ex){
                            isStyle = false;
                        }
                        if(isStyle){
                            defaultStyle = e.getAttributeValue("style");
                            if(!defaultStyle.endsWith(";"))
                                defaultStyle += ";";
                        }
                        e.setAttribute("style", defaultStyle+"display: none !important;");
                        e.getParentElement().addContent(data);
                        Writer fw = new OutputStreamWriter(new FileOutputStream(html), StandardCharsets.UTF_8);
                        XMLOutputter xout = new XMLOutputter(Format.getPrettyFormat().setEncoding("UTF-8"));

                        xout.output(doc, fw);
                        this.reload();
                        this._selectionTagName = "data";
                        this._selectionId = data.getAttributeValue("id");
                        defaultAll(click_x, click_y);
                    }
                }

            }else {
                Text content = new Text(newAlt);
                e.removeContent();
                e.addContent(content);
                Element img = e.getParentElement().getChild("img", e.getNamespace());
                img.setAttribute("alt", newAlt);
                Writer fw = new OutputStreamWriter(new FileOutputStream(html), StandardCharsets.UTF_8);
                XMLOutputter xout = new XMLOutputter(Format.getPrettyFormat().setEncoding("UTF-8"));

                xout.output(doc, fw);
                this.reload();
                this._selectionTagName = "data";
                this._selectionId = e.getAttributeValue("id");

            }


        }catch (Exception e){
            System.out.println(e);
        }
    }

    /**
     * @author Julien Laffely
     * Replace l'image original et supprime la balise de texte qui avait été créer pour remplacer l'image
     */
    public void setImage(){
        try {
            File html = new File(main.epub.getPage(this.numpage));
            Document doc = new SAXBuilder().build(html);
            XPathExpression<org.jdom2.Element> xpe = XPathFactory.instance().compile(
                    "//xhtml:*[@id='" + this._selectionId + "']", Filters.element(), null, Namespace.getNamespace("xhtml", "http://www.w3.org/1999/xhtml")
            );
            Element e = xpe.evaluateFirst(doc);

            if(e.getName().equals("img")){
                String newStyle = e.getAttributeValue("style").replace("display: none !important;", "");
                if(newStyle.equals(""))
                    e.removeAttribute("style");
                else
                    e.setAttribute("style", newStyle);

                e.getParentElement().removeChild("data");
            }else{
                List<Element> children = e.getParentElement().getChildren();
                e = null;
                for(Element child : children){
                    if(child.getName().equals("img")) {
                        e = child;

                    }
                    if(child.getName().equals("data")) {
                        child.detach();
                    }
                }
                if(e == null)
                    throw new Exception("img not found");

                this._selectionTagName = e.getName();
                this._selectionId = e.getAttributeValue("id");
                String newStyle = e.getAttributeValue("style").replace("display: none !important;", "");
                if(newStyle.equals(""))
                    e.removeAttribute("style");
                else
                    e.setAttribute("style", newStyle);


            }
            Writer fw = new OutputStreamWriter(new FileOutputStream(html), StandardCharsets.UTF_8);
            XMLOutputter xout = new XMLOutputter(Format.getPrettyFormat().setEncoding("UTF-8"));

            xout.output(doc, fw);
            this.reload();
        }catch(Exception e){
            System.out.println(e);
        }

    }

    /**
     * @author Julien Laffely
     * Méthode qui permet de concerver l'historique après un changement de page
     */
    public void replaceHistory(){
        if(!this.saved_history.isEmpty()) {
            for (Quintet<String, String, String, String, String> t : this.saved_history) {
                this._selectionTagName = t.getValue2();
                change_CSSRule(t.getValue0(), t.getValue4());
                this.addStyleSheets(t.getValue1(), t.getValue3(), t.getValue0(), t.getValue4(),false);
            }

            this.selectionStyleSheets(id_previous_stylesheets);
            this._selectionTagName = "";
        }
    }

}


/*
Polices importés,
Polices PC
*/