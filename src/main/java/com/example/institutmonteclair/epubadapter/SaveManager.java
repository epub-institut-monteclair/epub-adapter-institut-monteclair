package com.example.institutmonteclair.epubadapter;

import java.io.*;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class SaveManager {

    private static SaveManager instance;

    private SaveManager() {
        Path p = this.getWorkspace();
        if(!p.toFile().exists()) {
            boolean isCreated = p.toFile().mkdir();
            if (!isCreated) {
                System.err.println("Can't create workspace folder");
            }

        }
        if(!this.getEpubWorkspace().toFile().exists()) {
            boolean isCreated = this.getEpubWorkspace().toFile().mkdir();
            if(!isCreated){
                System.err.println("Can't create workspace folder");
            }

        }
        if(!this.getFontsWorkspace().toFile().exists()) {
            boolean isCreated = this.getFontsWorkspace().toFile().mkdir();
            if(!isCreated){
                System.err.println("Can't create workspace folder");
            }
            try {
                File[] files = (new File(getClass().getResource("fonts").toURI())).listFiles();
                for (File f:files) {
                    Path copiedPath = this.getFontsWorkspace().resolve(f.getName());
                    Path originPath = Paths.get(f.getPath());
                    Files.copy(originPath, copiedPath);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if(!this.getHelp().toFile().exists()) {

            try {
                InputStream input = getClass().getResourceAsStream("help.html");
                OutputStream output = new FileOutputStream(new File(this.getHelp().toString()));
                int readBytes;
                byte[] buffer = new byte[4096];
                while ((readBytes = input.read(buffer)) > 0) {
                    output.write(buffer, 0, readBytes);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static SaveManager getInstance(){
        if (instance == null){
            instance = new SaveManager();
        }
        return instance;
    }

    public Path getWorkspace(){
        return Paths.get(System.getProperty("user.home"), ".epub");
    }

    public Path getEpubWorkspace(){
        return Paths.get(this.getWorkspace().toString(), "EPUB");
    }

    public Path getFontsWorkspace(){
        return Paths.get(this.getWorkspace().toString(), "FONTS");
    }

    public Path getHelp(){
        return Paths.get(this.getWorkspace().toString(), "help.html");
    }


    public boolean workspaceIsEmpty(){
        if (Files.isDirectory(SaveManager.getInstance().getWorkspace())) {
            try (Stream<Path> entries = Files.list(SaveManager.getInstance().getWorkspace())) {
                return !entries.findFirst().isPresent();
            } catch (IOException ignored) {
            }
        }
        return false;
    }

    public void deleteEpubWorkspace(){
        deleteFolder(new File(this.getEpubWorkspace().toString()));
    }

    private void deleteFolder(File folder) {
        File[] files = folder.listFiles();
        if(files!=null) {
            for(File f: files) {
                if(f.isDirectory()) {
                    deleteFolder(f);
                } else {
                    f.delete();
                }
            }
        }
        if (!folder.getName().equals("EPUB")) {
            folder.delete();
        }
    }

}
